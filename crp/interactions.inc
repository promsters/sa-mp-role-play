stock InteractionRequest(playerid)
{
	if( GetTickCount() - pInfo[playerid][player_last_intact_press] < 1200) return;

	pInfo[playerid][player_last_intact_press] = GetTickCount();

	new object_id, pretender_id = -1, Float:pretender_distance = 2.5, Float:distance;
	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);

	for(new player_object=0;player_object<Streamer_GetVisibleItems(STREAMER_TYPE_OBJECT, playerid);player_object++)
 	{
        if( IsValidPlayerObject(playerid, player_object) )
     	{
            object_id = Streamer_GetItemStreamerID(playerid, STREAMER_TYPE_OBJECT, player_object);

            // if object type is interactable
            if(!Iter_Contains(Objects, object_id)) continue;

            if( IsObjectInteractable(object_id) && GetPlayerVirtualWorld(playerid) == Object[object_id][object_vw] )
           	{
                Streamer_GetDistanceToItem(x, y, z, STREAMER_TYPE_OBJECT, object_id, distance);
                if( distance < pretender_distance )
                {
                    pretender_distance = distance;
                    pretender_id = object_id;
                }
         	}
        }
    }

    if( pretender_id > -1 )
    {
    	ProcessInteraction(playerid, pretender_id);
    }
    else
    {
    	SendPlayerInformation(playerid, "W poblizu nie ma dostepnych interakcji", 2000);
    }
}

stock IsObjectInteractable(objectid)
{
	new otype = Object[objectid][object_type];
	if( otype == OBJECT_TYPE_GYM_BENCH ) return true;

	return false;
}

stock ProcessInteraction(playerid, objectid)
{
	switch(Object[objectid][object_type])
	{
		case OBJECT_TYPE_GYM_BENCH:
		{
			GymStartBench(playerid, objectid);
		}
	}
}

stock GetClosestObjectType(playerid, object_mod)
{
    new object_id,
        Float:prevdist = 5.0, OID = INVALID_OBJECT_ID, Float:PosX, Float:PosY, Float:PosZ, Float:dist;

    GetPlayerPos(playerid, PosX, PosY, PosZ);
     
    return OID;
}