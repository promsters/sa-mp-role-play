stock AddPlayerPenalty(playerid, type, giverid, endtime, reason[], extra_data = -1, length = sizeof reason)
{
	if( giverid == INVALID_PLAYER_ID )
	{
		if( HasCrewFlag(playerid, CREW_FLAG_ADMIN) || HasCrewFlag(playerid, CREW_FLAG_ADMIN_ROOT) )
		{
			if( pInfo[playerid][player_admin_duty] ) return;
		}
	}

	if( type == PENALTY_TYPE_BLOCK ) {
		// dezaktywujemy stare blokady o tym samym typie
		mysql_query(sprintf("UPDATE crp_penalties SET penalty_deactivate = 1 WHERE penalty_type = %d AND penalty_extraid = %d AND penalty_owner = %d", type, extra_data, pInfo[playerid][player_id]));
	}

	if( giverid != INVALID_PLAYER_ID )
	{
		mysql_query(sprintf("INSERT INTO `crp_penalties` (penalty_uid, penalty_type, penalty_owner, penalty_giver, penalty_date, penalty_end, penalty_reason, penalty_extraid) VALUES(null, %d, %d, %d, %d, %d, '%s', %d)", type, pInfo[playerid][player_id], pInfo[giverid][player_id], gettime(), endtime, reason, extra_data));
	}
	else 
	{
		if( type != PENALTY_TYPE_AJ ) mysql_query(sprintf("INSERT INTO `crp_penalties` (penalty_uid, penalty_type, penalty_owner, penalty_giver, penalty_date, penalty_end, penalty_reason, penalty_extraid) VALUES(null, %d, %d, %d, %d, %d, '%s', %d)", type, pInfo[playerid][player_id], -1, gettime(), endtime, reason, extra_data));
	}
	
	// Nick daj�cego
	new giver_name[MAX_PLAYER_NAME+1];
	if( giverid == INVALID_PLAYER_ID ) format(giver_name, sizeof(giver_name), "System");
	else format(giver_name, sizeof(giver_name), "%s", pInfo[giverid][player_name]);
	
	// Pow�d
	if( isnull(reason) ) format(reason, length, "brak");
		
	// Informacja
	switch(type)
	{
		case PENALTY_TYPE_KICK:
		{
			DisplayPenaltyInformation("Kick", giver_name, pInfo[playerid][player_name], reason);
			
			Kick(playerid);
		}
		
		case PENALTY_TYPE_WARN:
		{		
			DisplayPenaltyInformation("Warn", giver_name, pInfo[playerid][player_name], reason);
		}
		
		case PENALTY_TYPE_BAN:
		{			
			mysql_query(sprintf("INSERT INTO `crp_bans` VALUES(null, %d, '%s', '', '%s', 0)", gInfo[playerid][global_id], gInfo[playerid][global_ip], reason));
			mysql_query(sprintf("UPDATE `crp_characters` SET `char_block` = char_block + 1 WHERE `char_uid` = %d", pInfo[playerid][player_id]));
		
			DisplayPenaltyInformation(sprintf("Ban (%d dni)", extra_data), giver_name, pInfo[playerid][player_name], reason);
			
			Kick(playerid);
		}
		
		case PENALTY_TYPE_AJ:
		{
			mysql_query(sprintf("UPDATE `crp_characters` SET `char_aj` = %d WHERE `char_uid` = %d", extra_data*60, pInfo[playerid][player_id]));
				
			DisplayPenaltyInformation(sprintf("Admin Jail (%d min)", extra_data), giver_name, pInfo[playerid][player_name], reason);
			
			pInfo[playerid][player_aj] = extra_data * 60;
			pInfo[playerid][player_aj_end_time] = pInfo[playerid][player_aj] + gettime();
			
			scrp_SpawnPlayer(playerid, true);
		}
		
		case PENALTY_TYPE_BLOCK:
		{
			new timestr[20];
			new penalty_time = endtime - gettime();

			if( penalty_time < 0 )
			{
				format(timestr, sizeof(timestr), "nigdy nie wygasa");
			}
			else
			{
				if( penalty_time < 60*60 )
				{
					format(timestr, sizeof(timestr), "%d minut", floatround(penalty_time/60, floatround_floor));
				}
				else if( penalty_time < 60*60*24 )
				{
					format(timestr, sizeof(timestr), "%d godzin", floatround(penalty_time/(60*60), floatround_floor));
				}
				else
				{
					format(timestr, sizeof(timestr), "%d dni", floatround(penalty_time/(60*60*24), floatround_floor));
				}
			}
			
			
			switch( extra_data )
			{
				case BLOCK_CHAR:
				{
					DisplayPenaltyInformation(sprintf("Blokada postaci (%s)", timestr), giver_name, pInfo[playerid][player_name], reason);
					
					Kick(playerid);
				}
				
				case BLOCK_OOC:
				{
					DisplayPenaltyInformation(sprintf("Blokada OOC (%s)", timestr), giver_name, pInfo[playerid][player_name], reason);
				}
				
				case BLOCK_VEHICLES:
				{
					DisplayPenaltyInformation(sprintf("Blokada prowadzenia pojazd�w (%s)", timestr), giver_name, pInfo[playerid][player_name], reason);
					
					new vid = GetPlayerVehicleID(playerid);
					if( vid > 0 )
					{
						if( GetPlayerVehicleSeat(playerid) == 0 ) RemovePlayerFromVehicle(playerid);
					}
				}
				
				case BLOCK_RUN:
				{
					DisplayPenaltyInformation(sprintf("Blokada biegania (%s)", timestr), giver_name, pInfo[playerid][player_name], reason);
				}
			}
			
			if( !PlayerHasBlock(playerid, extra_data) ) pInfo[playerid][player_block] += extra_data;
			setPlayerBlockTime(playerid, extra_data, endtime);
		}
		
		case PENALTY_TYPE_GS:
		{
			GivePlayercPoints(playerid, extra_data);
		
			if( extra_data > 0 ) DisplayPenaltyInformation(sprintf("Punkty GameScore (+%d)", extra_data), giver_name, pInfo[playerid][player_name], reason);
			else DisplayPenaltyInformation(sprintf("Punkty GameScore (-%d)", extra_data), giver_name, pInfo[playerid][player_name], reason);
		}
	}
	
	SendClientMessage(playerid, COLOR_RED, "Je�eli kara zosta�a nadana nies�usznie - mo�esz apelowa� na naszym forum.");
	SendClientMessage(playerid, COLOR_RED, "Wszystkie nadane kary s� logowane i znale�� je mo�esz w swoim profilu na stronie.");
	SendClientMessage(playerid, COLOR_RED, "Adres naszej strony: www.c-games.net. Pami�taj, by nie za�atwia� takich spraw w grze!");
}

stock DisplayPenaltyInformation(header[], giver_name[], obtainer_name[], reason[])
{	
	format(PenaltyQueue[pq_header], 60, header);
	format(PenaltyQueue[pq_giver_name], MAX_PLAYER_NAME+1, "%s", giver_name);
	format(PenaltyQueue[pq_obtainer_name], MAX_PLAYER_NAME+1, "%s", obtainer_name);
	
	replacePolishChars(reason);
	format(PenaltyQueue[pq_reason], 150, "%s", reason);	

	ShowPenalty();
}

stock ShowPenalty()
{
	if( IsPenaltyDisplayed ) 
	{
		HidePenalty(PenaltyHideSecure);
		PenaltyHideSecure ++;
	}
	else
	{
		PenaltyHideSecure = 0;
	}
	
	// Formatowanie
	TextDrawSetString(PenaltiesTextDraw[0], PenaltyQueue[pq_header]);
	if( strlen(PenaltyQueue[pq_reason]) > 0 ) TextDrawSetString(PenaltiesTextDraw[1], sprintf("Nadawca: %s~n~Odbiorca: %s~n~~n~Powod:~w~~n~%s", PenaltyQueue[pq_giver_name], PenaltyQueue[pq_obtainer_name], BreakLines(PenaltyQueue[pq_reason], "~n~", 60)));
	else TextDrawSetString(PenaltiesTextDraw[1], sprintf("Nadawca: %s~n~Odbiorca: %s", PenaltyQueue[pq_giver_name], PenaltyQueue[pq_obtainer_name]));
	// Wyswietlenie
	foreach(new p : Player)
	{
		if( pInfo[p][player_logged] )
		{
			for(new i=0;i<2;i++) TextDrawShowForPlayer(p, PenaltiesTextDraw[i]);
		}
	}
	
	IsPenaltyDisplayed = true;
	
	defer HidePenalty[PENALTY_DISPLAY_TIME](PenaltyHideSecure);
}

timer HidePenalty[10](secure)
{
	if( secure != PenaltyHideSecure ) return;
	
	IsPenaltyDisplayed = false;
	
	// Ukrywamy
	foreach(new p : Player)
	{
		for(new i=0;i<2;i++) TextDrawHideForPlayer(p, PenaltiesTextDraw[i]);
	}
}