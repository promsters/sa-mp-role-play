stock LoadConfiguration()
{
	Code_ExTimer_Begin(LoadConfiguration);
	
	if( !dini_Exists(CONFIG_FILEPATH) ) 
	{
		dini_Create(CONFIG_FILEPATH);
		
		dini_Set(CONFIG_FILEPATH, "mysql_hostname", "127.0.0.1");
		dini_Set(CONFIG_FILEPATH, "mysql_username", "admin");
		dini_Set(CONFIG_FILEPATH, "mysql_password", "admin");
		dini_Set(CONFIG_FILEPATH, "mysql_database", "red");
	}
	
	dini_Get(CONFIG_FILEPATH, "mysql_hostname", Setting[setting_mysql_hostname]);
	dini_Get(CONFIG_FILEPATH, "mysql_username", Setting[setting_mysql_username]);
	dini_Get(CONFIG_FILEPATH, "mysql_password", Setting[setting_mysql_password]);
	dini_Get(CONFIG_FILEPATH, "mysql_database", Setting[setting_mysql_database]);
	
	Setting[setting_server_hour] = -1;
	Setting[setting_server_weather] = 1;
	
	SetGameModeText("iStory "CRP_HUMAN_VERSION"");
	
	printf("[crp] Wczytano konfiguracj� serwera [czas wykonania: %d ms]", Code_ExTimer_End(LoadConfiguration));
}

new LogPrintfStr[200];
#define logprintf(%0,%1,%2) \
format(LogPrintfStr, 200, %1, %2); \
_logprintf(%0, LogPrintfStr)

stock _logprintf(const file_name[], const string[])
{
	new File:log = fopen(file_name, io_append);
    if(log)
    {
		new date_time[60], Year, Month, Day, Hour, Minute, Second;
		gettime(Hour, Minute, Second);
		getdate(Year, Month, Day);	
		format(date_time, sizeof(date_time), "[%02d.%02d.%02d, %02d:%02d:%02d] ", Day, Month, Year, Hour, Minute, Second);
		
		fwrite(log, date_time);
        fwrite(log, string);
		fwrite(log, "\r\n");
        fclose(log);
    }
}

stock ConnectMysql()
{
	Code_ExTimer_Begin(ConnectMysql);
	
	mySQlconnection = mysql_init();
	
	new 
		bool:connected = !!mysql_connect(Setting[setting_mysql_hostname], Setting[setting_mysql_username], Setting[setting_mysql_password], Setting[setting_mysql_database], mySQlconnection, 1);

	if( !connected )
	{
		printf("[crp] B��d po��czenia z baz� danych [czas trwania: %d ms]", Code_ExTimer_End(ConnectMysql));
		return false;
	}

	mysql_query("SET NAMES 'cp1250'");
	
	Setting[setting_mysql_last_reconnect] = gettime();
	
	printf("[crp] Po��czano z baz� danych %s@%s [czas trwania: %d ms]", Setting[setting_mysql_username], Setting[setting_mysql_hostname], Code_ExTimer_End(ConnectMysql));
	return true;
}

stock LoadGlobalSpawns()
{
	Code_ExTimer_Begin(LoadGlobalSpawns);
	
	mysql_query("SELECT * FROM crp_gspawns");
	mysql_store_result();
	for(new i; i < mysql_num_rows(); i++)
	{
		if( i >= MAX_GLOBAL_SPAWNS ) break;
		
		mysql_data_seek(i);
		mysql_fetch_row_data();
				
		GlobalSpawn[i][gspawn_id] = mysql_fetch_field_int("gspawn_uid");
		GlobalSpawn[i][gspawn_pos][0] = mysql_fetch_field_float("gspawn_posx");
		GlobalSpawn[i][gspawn_pos][1] = mysql_fetch_field_float("gspawn_posy");
		GlobalSpawn[i][gspawn_pos][2] = mysql_fetch_field_float("gspawn_posz");
		GlobalSpawn[i][gspawn_pos][3] = mysql_fetch_field_float("gspawn_posa");
		
		Iter_Add(GlobalSpawns, i);
		
		// Textdraw na spawnie
		GlobalSpawn[i][gspawn_label] = CreateDynamic3DTextLabel("", COLOR_WHITE, GlobalSpawn[i][gspawn_pos][0], GlobalSpawn[i][gspawn_pos][1], GlobalSpawn[i][gspawn_pos][2], 15.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, 0, 0, 0);
		Streamer_RemoveArrayData(STREAMER_TYPE_3D_TEXT_LABEL, GlobalSpawn[i][gspawn_label], E_STREAMER_PLAYER_ID, 0);
		
		new str[50];
		format(str, sizeof(str), "Globalny spawn (ID: %d)", i);
		UpdateDynamic3DTextLabelText(GlobalSpawn[i][gspawn_label], 0xFFFFFF80, str);
	}
	
	printf("[crp] Proces wczytywania globalnych spawn�w zako�czony pomy�lnie [wczytanych globalnych spawn�w: %d] [czas trwania: %d ms]", Iter_Count(GlobalSpawns), Code_ExTimer_End(LoadGlobalSpawns));
	mysql_free_result();
}

stock LoadObjects()
{
	Code_ExTimer_Begin(LoadObjects);
	
	new loaded = LoadObject();
	
	printf("[crp] Proces wczytywania obiekt�w zako�czony pomy�lnie [wczytanych obiekt�w: %d] [czas trwania: %d ms]", loaded, Code_ExTimer_End(LoadObjects));
}

stock LoadGroups()
{
	Code_ExTimer_Begin(LoadGroups);
	
	mysql_query("SELECT * FROM `crp_groups` ORDER BY `group_owner` ASC");  
	mysql_store_result();
	for(new i;i<mysql_num_rows();i++)
	{
		mysql_data_seek(i);
		mysql_fetch_row_data();
		
		Group[i][group_uid] = mysql_fetch_field_int("group_uid");
		Group[i][group_type] = mysql_fetch_field_int("group_type");
		Group[i][group_parent_uid] = mysql_fetch_field_int("group_owner");
		Group[i][group_flags] = mysql_fetch_field_int("group_flags");

		new gid = GetGroupByUid(Group[i][group_parent_uid]);
		if( gid != -1 )
		{
			Group[i][group_flags] = Group[gid][group_flags];
		}
		
		Group[i][group_bank_money] = mysql_fetch_field_int("group_cash");
		mysql_fetch_field("group_name", Group[i][group_name]);
		
		Group[i][group_activity_points] = mysql_fetch_field_int("group_activity");
		Group[i][group_value1] = mysql_fetch_field_int("group_value1");
		Group[i][group_value2] = mysql_fetch_field_int("group_value2");
		
		Group[i][group_ooc] = !!mysql_fetch_field_int("group_ooc");
		
		Iter_Add(Groups, i);
	}
	
	printf("[crp] Proces wczytywania grup zako�czony pomy�lnie [wczytanych grup: %d] [czas trwania: %d ms]", mysql_num_rows(), Code_ExTimer_End(LoadGroups));
	mysql_free_result();
}

stock LoadDoors()
{
	Code_ExTimer_Begin(LoadDoors);
	
	new loaded = LoadDoor();
	
	printf("[crp] Proces wczytywania drzwi zako�czony pomy�lnie [wczytanych drzwi: %d] [czas trwania: %d ms]", loaded, Code_ExTimer_End(LoadDoors));
}

stock LoadAreas()
{
	Code_ExTimer_Begin(LoadAreas);
	
	new loaded = LoadArea();
	
	printf("[crp] Proces wczytywania stref zako�czony pomy�lnie [wczytanych stref: %d] [czas trwania: %d ms]", loaded, Code_ExTimer_End(LoadAreas));
}

stock LoadLabels()
{
	Code_ExTimer_Begin(LoadLabels);
	
	new loaded = LoadLabel();
	
	printf("[crp] Proces wczytywania tekst�w 3d zako�czony pomy�lnie [wczytanych tekst�w 3d: %d] [czas trwania: %d ms]", loaded, Code_ExTimer_End(LoadLabels));
}

stock LoadVehicles()
{
	Code_ExTimer_Begin(LoadVehicles);

	new loaded = LoadVehicle(sprintf("WHERE `vehicle_ownertype` = %d OR vehicle_spawn_restart = 1", VEHICLE_OWNER_TYPE_GROUP));
	
	printf("[crp] Proces wczytywania pojazd�w zako�czony pomy�lnie [wczytanych pojazd�w: %d] [czas trwania: %d ms]", loaded, Code_ExTimer_End(LoadVehicles));
}

stock LoadItems()
{
	Code_ExTimer_Begin(LoadItems);

	new loaded = LoadItem(sprintf("WHERE `item_ownertype` = %d OR `item_ownertype` = %d OR `item_ownertype` = %d OR `item_ownertype` = %d OR `item_ownertype` = %d", ITEM_OWNER_TYPE_GROUND, ITEM_OWNER_TYPE_DOOR, ITEM_OWNER_TYPE_DOOR_WAREHOUSE, ITEM_OWNER_TYPE_VEHICLE, ITEM_OWNER_TYPE_VEHICLE_COMPONENT));
	
	printf("[crp] Proces wczytywania przedmiot�w zako�czony pomy�lnie [wczytanych przedmiot�w: %d] [czas trwania: %d ms]", loaded, Code_ExTimer_End(LoadItems));
}

stock LoadSkins()
{
	Iter_Init(Skins);
	mysql_query("SELECT * FROM crp_skins WHERE skin_group = 0 AND skin_price > 0");
	mysql_store_result();
	
	for(new i;i<mysql_num_rows();i++)
	{
		mysql_data_seek(i);
		mysql_fetch_row_data();
		
		Skin[i][skin_value] = mysql_fetch_field_int("skin_id");
		Skin[i][skin_price] = mysql_fetch_field_int("skin_price");
		
		if( mysql_fetch_field_int("skin_sex") == 0 ) Iter_Add(Skins[0], i);
		else Iter_Add(Skins[1], i);
	}
	
	mysql_free_result();
}

stock LoadBuses()
{
	mysql_query("SELECT * FROM crp_buses");
	mysql_store_result();
	
	for(new i;i<mysql_num_rows();i++)
	{
		mysql_data_seek(i);
		mysql_fetch_row_data();
		
		Bus[i][bus_uid] = mysql_fetch_field_int("bus_uid");
		Bus[i][bus_objectid] = INVALID_OBJECT_ID;
		Bus[i][bus_ratio] = mysql_fetch_field_float("bus_ratio");
		mysql_fetch_field("bus_name", Bus[i][bus_name]);
		
		Iter_Add(Buses, i);
	}
	
	mysql_free_result();
}

stock getBlockTypeName(block_type, dest[])
{
	for(new i;i<sizeof(BlockBit);i++)
	{
		if(BlockBit[i] == block_type)
		{
			strcopy(dest, BlockName[i]);
			return;
		}
	}

	strcopy(dest, "BEZ_NAZWY");
	return;
}

task ServerTask[500]()
{
	if( players_task_last_execution == 0 ) players_task_last_execution = gettime();
	mysql_ping();
	
	if( Setting[setting_lsn_ad_finish_time] > 0 && Setting[setting_lsn_ad_finish_time] <= gettime() )
	{
		Setting[setting_lsn_ad_finish_time] = 0;
		TextDrawSetString(LSNtd, "~y~~h~LSN ~>~ ~w~Brak sygnalu nadawania.");
	}
	
	new yr, mo, dy;
	getdate(yr, mo, dy);
	
	TextDrawSetString(CRPversion, sprintf("istory roleplay~n~~w~ver. 1.0   %02d.%02d.%d", dy, mo, yr));
	
	new hour, minute;
	gettime(hour, minute);
	
	TextDrawSetString(ZegarekTD, sprintf("%02d:%02d", hour, minute));
	
	if( Setting[setting_server_hour] == -1 )
	{
		hour += 1;
		if( hour > 24 ) hour = hour-24;
		SetWorldTime(hour);
	}
	
	SetWeather(2);
	
	gettime(hour, minute);
	if( hour == 3 && minute == 0 )
	{
		new str[600];
		strcat(str, "UPDATE crp_characters c SET c.char_bankcash = c.char_bankcash + (SELECT sum(cg.group_payment) as cash FROM crp_char_groups cg LEFT JOIN crp_groups cgr ON cg.group_belongs = cgr.group_uid WHERE cg.char_uid = c.char_uid AND cg.group_payday = 1 AND cgr.group_activity >= 500 GROUP BY cg.char_uid)");
		strcat(str, " WHERE (SELECT sum(cg.group_payment) as cash FROM crp_char_groups cg LEFT JOIN crp_groups cgr ON cg.group_belongs = cgr.group_uid WHERE cg.char_uid = c.char_uid AND cg.group_payday = 1 AND cgr.group_activity >= 500 GROUP BY cg.char_uid) > 0");
		mysql_query(str);
		mysql_query("UPDATE crp_char_groups SET group_payday = 0");
		mysql_query("UPDATE crp_groups SET group_activity = 0");
	
		logprintf(LOG_DEBUG, "[DEBUGLOG] </nocny restart> %dh %dmin.", hour, minute);
		
		foreach(new p : Player)
		{
			SavePlayer(p);
		}
		
		foreach(new v : Vehicles)
		{
			SaveVehicle(v);
		}
		
		SendRconCommand("changemode crp");
	}
	
	// -- firedep.inc -- //
	ProcessRandomFires();

	if( gettime() - players_task_last_execution >= 1 ) 
	{
		PlayersTask();
		players_task_last_execution = gettime();
	}

	if( gettime() - antycrasher_task_last_execution >= 5 )
	{
		antycrasher_task_last_execution = gettime();
		mysql_query(sprintf("UPDATE anty_crasher SET `update` = %d WHERE id = 1", gettime()));
	}
	
	VehiclesTask();
}