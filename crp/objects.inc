stock LoadObject(limit[] = "", bool:return_id = false)
{
	mysql_query(sprintf("SELECT crp_objects.*, IF(mat_uid IS NULL, FALSE, TRUE) as has_materials FROM crp_objects LEFT JOIN crp_objects_mats ON mat_object = object_uid %s GROUP BY object_uid ", limit));  
	mysql_store_result();

	new rows = mysql_num_rows(), oid;

	new Iterator:loadMaterials<MAX_ITEMS>;

	new to_delete[MAX_OBJECTS], deleteid = 0;
	for(new k;k<MAX_OBJECTS;k++) to_delete[k] = -1;
	
	for(new i;i<rows;i++)
	{
		mysql_data_seek(i);
		mysql_fetch_row_data();

		new model = mysql_fetch_field_int("object_model"),
			vw = mysql_fetch_field_int("object_world"),
			owner = mysql_fetch_field_int("object_owner"),
			owner_type = mysql_fetch_field_int("object_ownertype");
			
		if( owner_type == 0 ) continue;
			
		oid = CreateDynamicObject(model, mysql_fetch_field_float("object_posx"), mysql_fetch_field_float("object_posy"), mysql_fetch_field_float("object_posz"), mysql_fetch_field_float("object_rotx"), mysql_fetch_field_float("object_roty"), mysql_fetch_field_float("object_rotz"), vw, -1, -1, 300.0);
		
		for(new z=0; e_objects:z != e_objects; z++)
		{
			Object[oid][e_objects:z] = 0;
		}
		
		Object[oid][object_uid] = mysql_fetch_field_int("object_uid");
		Object[oid][object_owner_type] = owner_type;
		Object[oid][object_owner] = owner;
		
		switch( Object[oid][object_owner_type] )
		{
			case OBJECT_OWNER_TYPE_AREA:
			{
				new a_id = GetAreaByUid(owner);
				if( a_id == -1 )
				{
					printf("[crp] Obiekt o UID: %d zosta� zniszczony poniewa� nie istnieje strefa do kt�rej nale�y.", Object[oid][object_uid]);
					to_delete[deleteid] = Object[oid][object_uid];
					deleteid++;
					DeleteObject(oid, false);
					continue;
				}
			}
			
			case OBJECT_OWNER_TYPE_DOOR:
			{
				new d_id = GetDoorByUid(owner);
				if( d_id == -1 )
				{
					printf("[crp] Obiekt o UID: %d zosta� zniszczony poniewa� nie istnieja drzwi do kt�rych nale�y.", Object[oid][object_uid]);
					to_delete[deleteid] = Object[oid][object_uid];
					deleteid++;
					DeleteObject(oid, false);
					continue;
				}
			}
		}
		
		Object[oid][object_vw] = vw;
		Object[oid][object_model] = model;
		Object[oid][object_stream_distance] = 300.0;
		Object[oid][object_type] = mysql_fetch_field_int("object_type");
		
		switch( Object[oid][object_type] )
		{
			case OBJECT_TYPE_ATM:
			{
				// Tworzymy strefe bankomatu
				Object[oid][object_area] = CreateDynamicSphere(mysql_fetch_field_float("object_posx"), mysql_fetch_field_float("object_posy"), mysql_fetch_field_float("object_posz"), 2.0, vw);
				Area[Object[oid][object_area]][area_uid] = -1;
				Area[Object[oid][object_area]][area_type] = AREA_TYPE_ATM;
				
				Iter_Add(Areas, Object[oid][object_area]);
				
				// Tworzymy 3d text bankomatu
				Object[oid][object_label] = CreateDynamic3DTextLabel("{00FF00}Bankomat\n"HEX_COLOR_WHITE"(( Wpisz /bankomat stojac bardzo blisko,\nby uzyc bankomatu. ))", COLOR_WHITE, mysql_fetch_field_float("object_posx"), mysql_fetch_field_float("object_posy"), mysql_fetch_field_float("object_posz")+1.0, 4.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, vw);
			}
			
			case OBJECT_TYPE_BUS:
			{
				new bid = GetBusByUid(mysql_fetch_field_int("object_extraid"));
				if( bid == -1 ) 
				{
					DeleteObject(oid, false);
					continue;
				}
				
				Bus[bid][bus_objectid] = oid;
				
				// Tworzymy strefe bankomatu
				Object[oid][object_area] = CreateDynamicSphere(mysql_fetch_field_float("object_posx"), mysql_fetch_field_float("object_posy"), mysql_fetch_field_float("object_posz"), 6.0, vw);
				Area[Object[oid][object_area]][area_uid] = -1;
				Area[Object[oid][object_area]][area_type] = AREA_TYPE_BUS;
				Area[Object[oid][object_area]][area_owner] = bid;
				
				Iter_Add(Areas, Object[oid][object_area]);
				
				// Tworzymy 3d text przystanku
				Object[oid][object_label] = CreateDynamic3DTextLabel(sprintf("{FFD000}#%d %s\n"HEX_COLOR_WHITE"(( Wpisz /bus aby wybra� miejsce podr�y. ))", Bus[bid][bus_uid], Bus[bid][bus_name]), COLOR_WHITE, mysql_fetch_field_float("object_posx"), mysql_fetch_field_float("object_posy"), mysql_fetch_field_float("object_posz")+0.5, 8.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, vw);

			}
			
			case OBJECT_TYPE_GATE:
			{
				Object[oid][object_gatec_pos][0] = mysql_fetch_field_float("object_posx");
				Object[oid][object_gatec_pos][1] = mysql_fetch_field_float("object_posy");
				Object[oid][object_gatec_pos][2] = mysql_fetch_field_float("object_posz");
				Object[oid][object_gatec_pos][3] = mysql_fetch_field_float("object_rotx");
				Object[oid][object_gatec_pos][4] = mysql_fetch_field_float("object_roty");
				Object[oid][object_gatec_pos][5] = mysql_fetch_field_float("object_rotz");
				
				Object[oid][object_gateo_pos][0] = mysql_fetch_field_float("object_gatex");
				Object[oid][object_gateo_pos][1] = mysql_fetch_field_float("object_gatey");
				Object[oid][object_gateo_pos][2] = mysql_fetch_field_float("object_gatez");
				Object[oid][object_gateo_pos][3] = mysql_fetch_field_float("object_gaterotx");
				Object[oid][object_gateo_pos][4] = mysql_fetch_field_float("object_gateroty");
				Object[oid][object_gateo_pos][5] = mysql_fetch_field_float("object_gaterotz");
			}

			case OBJECT_TYPE_DRUG_COOKER:
			{
				// Tworzymy strefe bankomatu
				Object[oid][object_drug_cooked_id] = -1;
				Object[oid][object_area] = CreateDynamicSphere(mysql_fetch_field_float("object_posx"), mysql_fetch_field_float("object_posy"), mysql_fetch_field_float("object_posz"), 2.0, vw);
				Area[Object[oid][object_area]][area_uid] = -1;
				Area[Object[oid][object_area]][area_type] = AREA_TYPE_DRUG_COOKER;
				Area[Object[oid][object_area]][area_owner] = oid;
				
				Iter_Add(Areas, Object[oid][object_area]);
				
				// Tworzymy 3d text bankomatu
				Object[oid][object_label] = CreateDynamic3DTextLabel("{00FF00}Stol alchemiczny\n"HEX_COLOR_WHITE"(( Wpisz /craft stojac bardzo blisko,\nby uzyc stolu. ))", COLOR_WHITE, mysql_fetch_field_float("object_posx"), mysql_fetch_field_float("object_posy"), mysql_fetch_field_float("object_posz")+1.0, 4.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, vw);
			}
		}
		
		// transformacja
		new trans = mysql_fetch_field_int("has_materials");

		if(trans) {
			Iter_Add(loadMaterials, oid);
		}
		
		Iter_Add(Objects, oid);
	}
	
	mysql_free_result();

	// usuwanie osieroconych obiektow
	if( deleteid > 0 )
	{
		new query_str[2000];
		format(query_str, sizeof(query_str), "DELETE FROM crp_objects WHERE object_uid IN(");
		for(new j;j<deleteid;j++)
		{
			if( to_delete[j] == -1 ) continue;

			format(query_str, sizeof(query_str), "%s%d,", query_str, to_delete[j]);
		}

		strdel(query_str, strlen(query_str)-1, strlen(query_str));
		format(query_str, sizeof(query_str), "%s)", query_str);

		print(query_str);

		mysql_query(query_str);
	}

	// wczytywanie mmat�w
	foreach(new o_id : loadMaterials)
	{
		mysql_query(sprintf("SELECT * FROM crp_objects_mats WHERE mat_object = %d", Object[o_id][object_uid]));
		mysql_store_result();

		rows = mysql_num_rows();
		
		for(new i;i<rows;i++)
		{
			mysql_data_seek(i);
			mysql_fetch_row_data();
			new data[100];
			new type = mysql_fetch_field_int("mat_type");
			new index = mysql_fetch_field_int("mat_index");
			mysql_fetch_field("mat_value", data);

			switch(type)
			{
				case 0:
				{
					new color[16], mmodel, txd_name[32], texture_name[32];
					sscanf(data, "p<:>s[16]ds[32]s[32]", color, mmodel, txd_name, texture_name);
					format(color, 16, "0x%s", color);

					if( !strcmp(color, "0x000000") ) SetDynamicObjectMaterial(o_id, index, mmodel, txd_name, texture_name, 0);
					else SetDynamicObjectMaterial(o_id, index, mmodel, txd_name, texture_name, HexToInt(color));
				}
				
				case 1:
				{
					new msize, fsize, bold, fcolor[16], bcolor[16], align, font[32], text[100];
					sscanf(data, "p<:>ddds[16]s[16]ds[32] s[50]", msize, fsize, bold, fcolor, bcolor, align, font, text);
					format(fcolor, 16, "0x%s", fcolor);
					
					if( strcmp(bcolor, "000000") != 0 ) format(bcolor, 16, "0x%s", bcolor);
					
					
					for(new y=0;y<text[y];y++)
					{
						if(text[y] == '|')
						{
							strdel(text, y, y+1);
							strins(text, "\n", y);
						}
					}
					
					strreplace(text, "(", "{");
					strreplace(text, ")", "}");
					
					if( strcmp(bcolor, "000000") != 0 ) SetDynamicObjectMaterialText(o_id, index, text, msize, font, fsize, bold, HexToInt(fcolor), HexToInt(bcolor), align);
					else SetDynamicObjectMaterialText(o_id, index, text, msize, font, fsize, bold, HexToInt(fcolor), 0, align);
				}
			}
		}

		mysql_free_result();
	}
	
	if( return_id ) return oid;
	return rows;
}

stock DeleteObject(o_id, bool:from_db = true)
{
	if( from_db )
	{
		mysql_query(sprintf("DELETE FROM `crp_objects` WHERE `object_uid` = %d", Object[o_id][object_uid]));
		mysql_query(sprintf("DELETE FROM crp_objects_mats WHERE mat_object= %d", Object[o_id][object_uid]));
	}
	
	DestroyDynamicObject(o_id);
	
	Iter_Remove(Objects, o_id);
	
	switch( Object[o_id][object_type] )
	{
		case OBJECT_TYPE_ATM, OBJECT_TYPE_BUS, OBJECT_TYPE_DRUG_COOKER:
		{
			DestroyDynamicArea(Object[o_id][object_area]);
			DestroyDynamic3DTextLabel(Object[o_id][object_label]);
		}
	}
	
	for(new z=0; e_objects:z != e_objects; z++)
    {
		Object[o_id][e_objects:z] = 0;
    }
	
	return 1;
}

stock IsObjectEdited(o_id)
{
	if( Object[o_id][object_is_edited] )
	{
		foreach(new p : Player)
		{
			if( pInfo[p][player_edited_object] == o_id ) return 1;
		}
	}
	
	return 0;
}

stock GetObjectDataForPlayer(playerid, &owner, &owner_type)
{
	if( GetPlayerVirtualWorld(playerid) == 0 )
	{
		new a_id = GetPlayerArea(playerid, AREA_TYPE_NORMAL);
		if( a_id != -1 )
		{
			if( HasCrewFlag(playerid, CREW_FLAG_EDITOR) || CanPlayerEditArea(playerid, a_id) )
			{
				owner = Area[a_id][area_uid];
				owner_type = OBJECT_OWNER_TYPE_AREA;
				
				return 1;
			}
		}
		
		if( HasCrewFlag(playerid, CREW_FLAG_EDITOR) )
		{
			owner = 0;
			owner_type = OBJECT_OWNER_TYPE_GLOBAL;
			
			return 1;
		}
	}
	else
	{
		new d_id = GetDoorByUid(GetPlayerVirtualWorld(playerid));
		
		if( d_id == -1 ) return 1;
		if( Door[d_id][door_type] != DOOR_TYPE_NORMAL ) return 1;
		
		if( HasCrewFlag(playerid, CREW_FLAG_EDITOR) )
		{
			owner = Door[d_id][door_uid];
			owner_type = OBJECT_OWNER_TYPE_DOOR;
			
			return 1;
		}
		
		if( CanPlayerEditDoor(playerid, d_id) )
		{
			owner = Door[d_id][door_uid];
			owner_type = OBJECT_OWNER_TYPE_DOOR;
			
			return 1;
		}
	}
	return 1;
}

stock CanPlayerEditObject(playerid, o_id)
{
	if( HasCrewFlag(playerid, CREW_FLAG_EDITOR) ) return 1;
	
	switch( Object[o_id][object_owner_type] )
	{	
		case OBJECT_OWNER_TYPE_DOOR:
		{
			if( CanPlayerEditDoor(playerid, GetDoorByUid(Object[o_id][object_owner])) ) return 1;
		}
		
		case OBJECT_OWNER_TYPE_AREA:
		{
			if( CanPlayerEditArea(playerid, GetAreaByUid(Object[o_id][object_owner])) ) return 1;
		}
	}
	return 0;
}

stock Float:GetPlayerDistanceToObject(playerid, object_id)
{
	if( !IsValidDynamicObject(object_id) ) return 0.0;
	
	new Float:p_pos[3], Float:distance;
	GetPlayerPos(playerid, p_pos[0], p_pos[1], p_pos[2]);
	
	Streamer_GetDistanceToItem(p_pos[0], p_pos[1], p_pos[2], STREAMER_TYPE_OBJECT, object_id, distance);
	return distance;
}

stock UpdateObjectInfoTextdraw(playerid, o_id)
{
	new string[200];

	switch( Object[o_id][object_owner_type] )
	{
		case OBJECT_OWNER_TYPE_GLOBAL: format(string, sizeof(string), "%d. Obiekt globalny (ID: %d)", Object[o_id][object_uid], o_id);
		case OBJECT_OWNER_TYPE_DOOR: format(string, sizeof(string), "%d. Obiekt drzwi (ID: %d)", Object[o_id][object_uid], o_id);
		case OBJECT_OWNER_TYPE_AREA: format(string, sizeof(string), "%d. Obiekt strefowy (ID: %d)", Object[o_id][object_uid], o_id);
	}

	format(string, sizeof(string), "%s~n~~n~~p~Owner: ~w~%d:%d    ~b~Typ: ~w~%d    Model: %d", string, Object[o_id][object_owner_type], Object[o_id][object_owner], Object[o_id][object_type], Object[o_id][object_model]);	
	format(string, sizeof(string), "%s~n~~r~Pozycja: ~w~%.2f, %.2f, %.2f~n~~r~Rotacja: ~w~%.2f, %.2f, %.2f", string, Object[o_id][object_pos][0], Object[o_id][object_pos][1], Object[o_id][object_pos][2], Object[o_id][object_pos][3], Object[o_id][object_pos][4], Object[o_id][object_pos][5]);

	PlayerTextDrawSetString(playerid, pInfo[playerid][Dashboard], string);
}

stock GetObjectByUid(uid)
{
	foreach(new o_id : Objects)
	{
		if( Object[o_id][object_uid] == uid ) return o_id;
	}
	return -1;
}

stock RemoveBuildingsForPlayer(playerid)
{
	RemoveBuildingForPlayer(playerid, 4026, 1497.7969, -1543.7109, 17.5547, 0.25);
	RemoveBuildingForPlayer(playerid, 4054, 1402.5000, -1682.0234, 25.5469, 0.25);
	RemoveBuildingForPlayer(playerid, 4218, 1497.7031, -1546.6172, 43.9922, 0.25);
	RemoveBuildingForPlayer(playerid, 620, 1419.3281, -1710.2344, 11.8359, 0.25);
	RemoveBuildingForPlayer(playerid, 4005, 1402.5000, -1682.0234, 25.5469, 0.25);
	RemoveBuildingForPlayer(playerid, 4016, 1497.7969, -1543.7109, 17.5547, 0.25);
	RemoveBuildingForPlayer(playerid, 17830, 2413.6875, -1576.6406, 16.2031, 0.25);
	RemoveBuildingForPlayer(playerid, 17868, 2458.3828, -1532.4375, 22.9922, 0.25);
	RemoveBuildingForPlayer(playerid, 1525, 2462.2656, -1541.4141, 25.4219, 0.25);
	RemoveBuildingForPlayer(playerid, 1261, 2445.2031, -1519.3906, 37.0547, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 2491.8281, -1516.5078, 23.2109, 0.25);
	RemoveBuildingForPlayer(playerid, 673, 2495.2656, -1538.0078, 22.8672, 0.25);
	RemoveBuildingForPlayer(playerid, 673, 2495.2656, -1553.0781, 22.8672, 0.25);
	RemoveBuildingForPlayer(playerid, 760, 2495.1563, -1549.7500, 23.3359, 0.25);
	RemoveBuildingForPlayer(playerid, 673, 2495.2656, -1522.8047, 22.8672, 0.25);
	RemoveBuildingForPlayer(playerid, 760, 2494.2109, -1521.6094, 23.3359, 0.25);
	RemoveBuildingForPlayer(playerid, 760, 2494.0078, -1529.2188, 23.3359, 0.25);
	RemoveBuildingForPlayer(playerid, 17916, 2366.8125, -1537.5391, 39.4688, 0.25);
	RemoveBuildingForPlayer(playerid, 17829, 2413.6875, -1576.6406, 16.2031, 0.25);
	RemoveBuildingForPlayer(playerid, 673, 2451.6641, -1561.5469, 23.3125, 0.25);
	RemoveBuildingForPlayer(playerid, 760, 2446.1563, -1562.2031, 23.3359, 0.25);
	RemoveBuildingForPlayer(playerid, 760, 2467.7969, -1562.2031, 23.3359, 0.25);
	RemoveBuildingForPlayer(playerid, 616, 2469.4141, -1562.1328, 22.5625, 0.25);
	RemoveBuildingForPlayer(playerid, 1297, 2459.2656, -1558.7266, 26.2031, 0.25);
	RemoveBuildingForPlayer(playerid, 620, 2445.7969, -1543.2578, 21.8047, 0.25);
	RemoveBuildingForPlayer(playerid, 760, 2444.6797, -1542.7813, 23.2031, 0.25);
	RemoveBuildingForPlayer(playerid, 620, 2457.0156, -1542.6953, 21.8047, 0.25);
	RemoveBuildingForPlayer(playerid, 1297, 2474.1016, -1544.2578, 26.3594, 0.25);
	RemoveBuildingForPlayer(playerid, 760, 2463.7344, -1542.7813, 23.2031, 0.25);
	RemoveBuildingForPlayer(playerid, 17863, 2467.4609, -1538.2500, 27.6016, 0.25);
	RemoveBuildingForPlayer(playerid, 17974, 2467.4609, -1538.2500, 27.6016, 0.25);
	RemoveBuildingForPlayer(playerid, 17862, 2458.3828, -1532.4375, 22.9922, 0.25);
	RemoveBuildingForPlayer(playerid, 1297, 2482.9297, -1530.5391, 26.3047, 0.25);
	RemoveBuildingForPlayer(playerid, 1267, 2445.2031, -1519.3906, 37.0547, 0.25);
	RemoveBuildingForPlayer(playerid, 1297, 2474.6328, -1518.7734, 26.3047, 0.25);
	RemoveBuildingForPlayer(playerid, 1297, 2459.2500, -1508.9609, 26.3047, 0.25);
}