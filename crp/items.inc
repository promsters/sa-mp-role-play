stock LoadItem(limit[] = "", bool:return_id = false)
{
	mysql_query(sprintf("SELECT * FROM `crp_items` %s", limit));  
	mysql_store_result();
	
	new i_id = -1, rows = mysql_num_rows();
	
	for(new i;i<mysql_num_rows();i++)
	{
		mysql_data_seek(i);
		mysql_fetch_row_data();
		
		i_id = Iter_Free(Items);
		Iter_Add(Items, i_id);
		
		Item[i_id][item_uid] = mysql_fetch_field_int("item_uid");
		Item[i_id][item_type] = mysql_fetch_field_int("item_type");
		Item[i_id][item_value1] = mysql_fetch_field_int("item_value1");
		Item[i_id][item_value2] = mysql_fetch_field_int("item_value2");
		Item[i_id][item_used] = mysql_fetch_field_int("item_used");
		Item[i_id][item_created] = mysql_fetch_field_int("item_created");
		
		Item[i_id][item_owner_type] = mysql_fetch_field_int("item_ownertype");
		Item[i_id][item_owner] = mysql_fetch_field_int("item_owner");
		Item[i_id][item_group] = mysql_fetch_field_int("item_group");
		
		Item[i_id][item_price] = mysql_fetch_field_int("item_price");
		Item[i_id][item_amount] = mysql_fetch_field_int("item_amount");
		
		Item[i_id][item_model] = mysql_fetch_field_int("item_model");
		Item[i_id][item_x] = mysql_fetch_field_float("item_posx");
		Item[i_id][item_y] = mysql_fetch_field_float("item_posy");
		Item[i_id][item_z] = mysql_fetch_field_float("item_posz");
		Item[i_id][item_rx] = mysql_fetch_field_float("item_rotx");
		Item[i_id][item_ry] = mysql_fetch_field_float("item_roty");
		Item[i_id][item_rz] = mysql_fetch_field_float("item_rotz");
		Item[i_id][item_world] = mysql_fetch_field_int("item_world");
		Item[i_id][item_interior] = mysql_fetch_field_int("item_interior");
		
		mysql_fetch_field("item_name", Item[i_id][item_name]);
				
		if( Item[i_id][item_owner_type] == ITEM_OWNER_TYPE_GROUND ) Item_Drop(i_id);
	}
	
	mysql_free_result();
	
	if( return_id ) return i_id;
	return rows;
}

stock DeleteItem(itemid, bool:from_db = false)
{
	if( IsValidDynamicObject(Item[itemid][item_object]) ) DestroyDynamicObject(Item[itemid][item_object]);
	
	Iter_Remove(Items, itemid);
	
    if( from_db ) mysql_query(sprintf("DELETE FROM `crp_items` WHERE `item_uid` = %d", Item[itemid][item_uid]));
    
	for(new z=0; e_items:z != e_items; z++)
    {
		Item[itemid][e_items:z] = 0;
    }
}

stock Item_Use(itemid, playerid = INVALID_PLAYER_ID)
{
	if( playerid != INVALID_PLAYER_ID && (Item[itemid][item_owner_type] != ITEM_OWNER_TYPE_PLAYER || Item[itemid][item_owner] != pInfo[playerid][player_id]) ) return 1;
	
	switch( Item[itemid][item_type] )
	{
		case ITEM_TYPE_WEAPON:
		{
			if( Item[itemid][item_used] )
			{
				new wslot = GetWeaponSlot(Item[itemid][item_value1]);
				Item[itemid][item_value2] = pWeapon[playerid][wslot][pw_ammo];

				// zapisywanie granat�w
				if(wslot == 8) {
					new wpid, wpammo;
					GetPlayerWeaponData(playerid, wslot, wpid, wpammo);
					if(wpammo < Item[itemid][item_value2])
					{
						Item[itemid][item_value2] = wpammo;
					}
				}
				
				Item[itemid][item_used] = false;
				
				if( pWeapon[playerid][wslot][pw_object_index] > -1 ) RemovePlayerAttachedObject(playerid, pWeapon[playerid][wslot][pw_object_index]);
				
				pWeapon[playerid][wslot][pw_itemid] = -1;
				pWeapon[playerid][wslot][pw_id] = -1;
				pWeapon[playerid][wslot][pw_ammo] = -1;
				pWeapon[playerid][wslot][pw_object_index] = -1;
				
				ResetPlayerWeapons(playerid);
				
				for(new i;i<13;i++)
				{
					if( pWeapon[playerid][i][pw_itemid] > -1 ) GivePlayerWeapon(playerid, pWeapon[playerid][i][pw_id], pWeapon[playerid][i][pw_ammo]);
				}
				
				mysql_query(sprintf("UPDATE `crp_items` SET `item_value2` = %d WHERE `item_uid` = %d", Item[itemid][item_value2], Item[itemid][item_uid]));
			}
			else
			{
				new wslot = GetWeaponSlot(Item[itemid][item_value1]);
				
				// Sprawdzamy czy jest amunicja
				if( Item[itemid][item_value2] == 0 && GetWeaponType(Item[itemid][item_value1]) != WEAPON_TYPE_MELEE ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "W tej broni nie ma amunicji.");
				
				// Sprawdzamy czy czasem nie ma juz broni tego samego typu
				new wtype = GetWeaponType(Item[itemid][item_value1]);
				for(new i;i<13;i++)
				{
					if( pWeapon[playerid][i][pw_itemid] > -1 && GetWeaponType(pWeapon[playerid][i][pw_id]) == wtype ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Masz ju� wyci�gni�t� inn� bro� tego samego typu.");
				}
				
				// Sprawdzamy czy czasem bron nie jest oflagowana
				if( Item[itemid][item_group] > 0 )
				{
					new gid = GetGroupByUid(Item[itemid][item_group]);
					if( GetPlayerGroupSlot(playerid, gid) == -1 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie masz uprawnie� do u�ywania tej broni.");
				}
				
				pWeapon[playerid][wslot][pw_itemid] = itemid;
				pWeapon[playerid][wslot][pw_id] = Item[itemid][item_value1];
				pWeapon[playerid][wslot][pw_ammo] = Item[itemid][item_value2];
				GivePlayerWeapon(playerid, Item[itemid][item_value1], Item[itemid][item_value2]);
				
				Item[itemid][item_used] = true;
			}
		}
		
		case ITEM_TYPE_GLOVES:
		{
			if( Item[itemid][item_used] )
			{
				Item[itemid][item_used] = false;
				RemovePlayerStatus(playerid, PLAYER_STATUS_GLOVES);
				
				if( Item[itemid][item_value1] == 0 ) DeleteItem(itemid, true);
			}
			else
			{
				if( Item[itemid][item_value1] == 0 )
				{
					DeleteItem(itemid, true);
					return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie mo�esz u�y� tego przedmiotu.");
				}
				if( GetPlayerUsedItem(playerid, ITEM_TYPE_GLOVES) > -1 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Masz ju� u�yty inny przedmiot tego typu.");
			
				AddPlayerStatus(playerid, PLAYER_STATUS_GLOVES);
				
				// Zabieramy jedno uzycie rekawiczek jesli nie jest nieskonczona (dla org. porzadkowych)
				if( Item[itemid][item_value1] != -1 )
				{
					Item[itemid][item_value1] -= 1;
					mysql_query(sprintf("UPDATE `crp_items` SET `item_value1` = %d WHERE `item_uid` = %d", Item[itemid][item_value1], Item[itemid][item_uid]));
				}
			}
		}
		
		case ITEM_TYPE_MASK:
		{
			if( Item[itemid][item_used] )
			{
				strcopy(pInfo[playerid][player_name], pInfo[playerid][player_duty_tmp], MAX_PLAYER_NAME+1);
				Item[itemid][item_used] = false;
				UpdatePlayerLabel(playerid);
				
				if( Item[itemid][item_value1] == 0 ) DeleteItem(itemid, true);
			}
			else
			{
				if( pInfo[playerid][player_admin_duty] ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Podczas s�u�by admina nie mo�esz u�ywa� tego przedmiotu.");
				if( Item[itemid][item_value1] == 0 )
				{
					DeleteItem(itemid, true);
					return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie mo�esz u�y� tego przedmiotu.");
				}
				if( GetPlayerUsedItem(playerid, ITEM_TYPE_MASK) > -1 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Masz ju� u�yty inny przedmiot tego typu.");
				
				new str[6];
				format(str, sizeof(str), "%d", pInfo[playerid][player_id]);
				strcopy(str, MD5_Hash(str), 6);
				strcopy(pInfo[playerid][player_duty_tmp], pInfo[playerid][player_name], MAX_PLAYER_NAME+1);
				strtoupper(str);
				
				Item[itemid][item_used] = true;
				
				format(pInfo[playerid][player_name], MAX_PLAYER_NAME+1, "Nieznajomy %s", str);
				UpdatePlayerLabel(playerid);
				
				// Zabieramy jedno uzycie maski jesli nie jest nieskonczona (dla org. porzadkowych)
				if( Item[itemid][item_value1] != -1 )
				{
					Item[itemid][item_value1] -= 1;
					mysql_query(sprintf("UPDATE `crp_items` SET `item_value1` = %d WHERE `item_uid` = %d", Item[itemid][item_value1], Item[itemid][item_uid]));
				}
			}
		}
		
		case ITEM_TYPE_AMMO:
		{
			if( !Item[itemid][item_used] )
			{
				DynamicGui_Init(playerid);
				DynamicGui_SetDialogValue(playerid, itemid);
				new count, string[200];
				foreach(new item : Items)
				{
					if( Item[item][item_owner_type] == ITEM_OWNER_TYPE_PLAYER && Item[item][item_owner] == pInfo[playerid][player_id] )
					{
						if( Item[item][item_type] == ITEM_TYPE_WEAPON && Item[item][item_value1] == Item[itemid][item_value1] && !Item[item][item_used] )
						{
							format(string, sizeof(string), "%s%d\t\t%s\n", string, Item[item][item_uid], Item[item][item_name]);
							DynamicGui_AddRow(playerid, item);
							count++;
						}
					}
				}
				
				if( count == 0 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie posiadasz �adnych nieu�ywanych broni do kt�rych pasuje ta amunicja.");
				else ShowPlayerDialog(playerid, DIALOG_USE_AMMO, DIALOG_STYLE_LIST, "Wybierz bro� do kt�rej za�adujesz amunicj�:", string, "Wybierz", "Zamknij");
			}
		}
		
		case ITEM_TYPE_PHONE:
		{
			if( Item[itemid][item_used] )
			{
				cmd_tel(playerid, "");
			}
			else
			{
				if( GetPlayerUsedItem(playerid, ITEM_TYPE_PHONE) > -1 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Masz ju� w��czony inny telefon.");
			
				if( Item[itemid][item_value1] == 0 )
				{
					new str[7];
					format(str, sizeof(str), "%04d", pInfo[playerid][player_id]);
					
					new length = 6 - strlen(str);
					
					if( length == 2 ) format(str, sizeof(str), "%02d%s", rand(10, 99), str);
					else if( length == 1 ) format(str, sizeof(str), "%d%s", rand(1, 9), str);
					
					Item[itemid][item_value1] = strval(str);
					
					mysql_query(sprintf("UPDATE `crp_items` SET `item_value1` = %d WHERE `item_uid` = %d", Item[itemid][item_value1], Item[itemid][item_uid]));
				}
				
				Item[itemid][item_used] = true;
				
				mysql_query(sprintf("UPDATE `crp_items` SET `item_used` = 1 WHERE `item_uid` = %d", Item[itemid][item_uid]));
				
				GameTextForPlayer(playerid, "~w~Telefon ~g~wlaczony", 3000, 3);
			}
		}
		
		case ITEM_TYPE_NOTEPAD:
		{
			pInfo[playerid][player_dialog_tmp1] = itemid;
			ShowPlayerDialog(playerid, DIALOG_NOTEPAD, DIALOG_STYLE_INPUT, "Tworzenie notatki", "W poni�szym polu podaj tre�c notatki:", "Gotowe", "Anuluj");
		}
		
		case ITEM_TYPE_CHIT:
		{
			mysql_query(sprintf("SELECT chit_text FROM `crp_chits` WHERE `chit_uid` = %d", Item[itemid][item_value1]));
			mysql_store_result();
			
			if( mysql_num_rows() == 1 )
			{
				new text[100], date[30];
				mysql_fetch_field("chit_text", text);
				
				GetFormattedDate(Item[itemid][item_created], date);
				
				SendGuiInformation(playerid, "Tre�� karteczki", sprintf("%s\n\nData utworzenia: %s", text, date));
				mysql_free_result();
			}
			else
			{
				mysql_free_result();
				
				DeleteItem(itemid, true);
			}
		}
		
		case ITEM_TYPE_CLOTH:
		{
			if( Item[itemid][item_used] )
			{
				Item[itemid][item_used] = false;
				mysql_query(sprintf("UPDATE crp_items SET item_used = 0 WHERE item_uid = %d", Item[itemid][item_uid]));
				
				pInfo[playerid][player_last_skin] = pInfo[playerid][player_skin];
				SetPlayerSkin(playerid, pInfo[playerid][player_last_skin]);
			}
			else
			{
				new iid = GetPlayerUsedItem(playerid, ITEM_TYPE_CLOTH);
				if( iid > -1 )
				{
					Item[iid][item_used] = false;
					mysql_query(sprintf("UPDATE crp_items SET item_used = 0 WHERE item_uid = %d", Item[iid][item_uid]));
				}
				
				Item[itemid][item_used] = true;
				mysql_query(sprintf("UPDATE crp_items SET item_used = 1 WHERE item_uid = %d", Item[itemid][item_uid]));
				
				pInfo[playerid][player_last_skin] = Item[itemid][item_value1];
				SetPlayerSkin(playerid, pInfo[playerid][player_last_skin]);
			}
		}
		
		case ITEM_TYPE_FOOD, ITEM_TYPE_DRINK:
		{			
			if( Item[itemid][item_value1] > 0 )
			{
				if( pInfo[playerid][player_health] < 30 ) return SendGuiInformation(playerid, "Informacja", "Stan Twojego zdrowia wymaga za�ycia odpowiednich lekarstw aby przywr�ci� pe�n� sprawno��.\nDop�ki nie zastosujesz odpowiednich lek�w to poziom Twojego zdrowia nie wzro�nie.");
				new waznosc = gettime() - Item[itemid][item_created];
				if( waznosc > 60*60 )
				{
					DeleteItem(itemid);
					SendGuiInformation(playerid, "Informacja", "Ten produkt jest ju� przeterminowany i nie mo�esz go spo�y� wi�c wyrzucasz go do kosza.");
					return 1;
				}
				if( pInfo[playerid][player_taken_damage] > 0 && gettime() - pInfo[playerid][player_taken_damage] < 180 ) return SendGuiInformation(playerid, "Informacja", "Nie mo�esz spo�ywa� jedzenia do 3 minut po otrzymaniu obra�e�.");
				
				pInfo[playerid][player_health] += Item[itemid][item_value1];
				if( pInfo[playerid][player_health] > 100 ) pInfo[playerid][player_health] = 100;
				
				SetPlayerHealth(playerid, floatround(pInfo[playerid][player_health]));
			}
			
			if( Item[itemid][item_type] == ITEM_TYPE_DRINK )
			{
				if( Item[itemid][item_value2] == 0 )
				{
					SetPlayerSpecialAction(playerid, SPECIAL_ACTION_DRINK_SPRUNK);
				}
				else if( Item[itemid][item_value2] > 0 && Item[itemid][item_value2] < 5 )
				{
					SetPlayerSpecialAction(playerid, SPECIAL_ACTION_DRINK_WINE);
					
					pInfo[playerid][player_drunk_time] = 30*60;
				}
				else
				{
					SetPlayerSpecialAction(playerid, SPECIAL_ACTION_DRINK_BEER);
					
					pInfo[playerid][player_drunk_time] = 60*60;
				}
			}
			
			ProxMessage(playerid, sprintf("spo�ywa %s.", Item[itemid][item_name]), PROX_ME);
			
			DeleteItem(itemid, true);
		}
		
		case ITEM_TYPE_PETY:
		{
			SetPlayerSpecialAction(playerid, SPECIAL_ACTION_SMOKE_CIGGY);
			
			Item[itemid][item_value1] -= 1;
			
			if( Item[itemid][item_value1] <= 0 ) DeleteItem(itemid, true);
		}
		
		case ITEM_TYPE_ZEGAREK:
		{
			if( Item[itemid][item_used] )
			{
				TextDrawHideForPlayer(playerid, ZegarekTD);
				
				Item[itemid][item_used] = false;
				mysql_query(sprintf("UPDATE crp_items SET item_used = 0 WHERE item_uid = %d", Item[itemid][item_uid]));
			}
			else
			{
				if( GetPlayerUsedItem(playerid, ITEM_TYPE_ZEGAREK) > -1 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Masz ju� u�yty inny przedmiot tego typu.");
				
				TextDrawShowForPlayer(playerid, ZegarekTD);
				
				Item[itemid][item_used] = true;
				mysql_query(sprintf("UPDATE crp_items SET item_used = 1 WHERE item_uid = %d", Item[itemid][item_uid]));
			}
		}
		
		case ITEM_TYPE_ACCESSORY:
		{
			if( Item[itemid][item_used] )
			{
				if( !IsPlayerAttachedObjectSlotUsed(playerid, Item[itemid][item_value2]) ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Podczas u�ywania przedmiotu wyst�pi� b��d.");
				
				RemovePlayerAttachedObject(playerid, Item[itemid][item_value2]);
			
				Item[itemid][item_used] = false;
				mysql_query(sprintf("UPDATE crp_items SET item_used = 0 WHERE item_uid = %d", Item[itemid][item_uid]));
			}
			else
			{
				new count = GetPlayerUsedItemCount(playerid, ITEM_TYPE_ACCESSORY);
				if( IsPlayerVip(playerid) && count >= 6 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Jako posiadacz konta premium mo�esz mie� za�o�one tylko 6 dodatk�w postaci jednocze�nie.");
				else if( !IsPlayerVip(playerid) && count >= 2 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Jako i� nie posiadasz konta premium, mo�esz mie� za�o�one tylko 2 dodatki postaci jednocze�nie.\nZakup konto premium aby zwi�kszy� limit a� do 6!");
				
				if( Item[itemid][item_value1] == 0 )
				{
					mysql_query(sprintf("INSERT INTO crp_access (access_uid, access_model, access_bone) VALUES (null, %d, %d)", Item[itemid][item_model], Item[itemid][item_value2]));
					
					Item[itemid][item_value1] = mysql_insert_id();
					Item[itemid][item_value2] = 0;
					
					mysql_query(sprintf("UPDATE crp_items SET item_value1 = %d, item_value2 = %d WHERE item_uid = %d", Item[itemid][item_value1], Item[itemid][item_value2], Item[itemid][item_uid]));
				}
				
				pInfo[playerid][player_dialog_tmp1] = itemid;
				
				ShowPlayerDialog(playerid, DIALOG_ACCESSORY, DIALOG_STYLE_MSGBOX, "Zak�adanie dodatku", "Czy chcesz dostosowa� pozycj� dodatku postaci?", "Tak", "Nie");
			}
		}
		
		case ITEM_TYPE_GYM_PASS:
		{
			if( Item[itemid][item_used] )
			{
			
			}
			else
			{
				
			}
		}
		
		case ITEM_TYPE_MEDICINE:
		{
			if( pInfo[playerid][player_health] >= 30 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie mo�esz u�y� lekarstwa, poniewa� Tw�j stan zdrowia tego nie wymaga.");
			
			pInfo[playerid][player_health] = 35;

			SetPlayerHealth(playerid, floatround(pInfo[playerid][player_health]));
			
			ProxMessage(playerid, sprintf("aplikuje lekarstwo %s.", Item[itemid][item_name]), PROX_ME);
			
			DeleteItem(itemid, true);
		}
		
		case ITEM_TYPE_CORPSE:
		{
			new dslot = GetPlayerDutySlot(playerid);
			if( dslot == -1 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie jeste� na s�u�bie w grupie, kt�ra mo�e przeprowadza� sekcje zw�ok.");
			
			new gid = pGroup[playerid][dslot][pg_id];
			if( gid == -1 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie jeste� na s�u�bie w grupie, kt�ra mo�e przeprowadza� sekcje zw�ok.");
			if( !GroupHasFlag(gid, GROUP_FLAG_EXAMINE_CORPSE) ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie jeste� na s�u�bie w grupie, kt�ra mo�e przeprowadza� sekcje zw�ok.");
			
			new did = GetDoorByUid(GetPlayerVirtualWorld(playerid));
			if( did == -1 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie znajdujesz si� w budynku grupy, w kt�rej jeste� na s�u�bie.");
			if( Door[did][door_owner_type] != DOOR_OWNER_TYPE_GROUP || Door[did][door_owner] != Group[gid][group_uid] ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie znajdujesz si� w budynku grupy, w kt�rej jeste� na s�u�bie.");
			
			mysql_query(sprintf("SELECT * FROM crp_corpses WHERE corpse_uid = %d", Item[itemid][item_value1]));
			mysql_store_result();
			
			if( mysql_num_rows() == 0 ) SendGuiInformation(playerid, "Wyst�pi� b��d", "Wyst�pi� nieoczekiwany b��d, powiniene� zg�osi� go do administracji.");
			else
			{
				new cuid = mysql_fetch_field_int("corpse_uid"), examined = mysql_fetch_field_int("corpse_examined");
				if( examined > gettime() )
				{
					mysql_free_result();
					
					new examine_date[50];
					GetRelativeDate(examined, examine_date);
					SendGuiInformation(playerid, "Informacja", sprintf("Zw�oki s� aktualnie badane przez specjalist�w. Przewidywany czas zako�czenia: %s.", examine_date));
				}
				else if( examined == 0 )
				{
					mysql_free_result();
					
					mysql_query(sprintf("UPDATE crp_corpses SET corpse_examined = %d WHERE corpse_uid = %d", gettime() + 24*60*60, cuid));
					
					SendGuiInformation(playerid, "Informacja", "Zw�oki zosta�y poddane sekcji, kt�ra zako�czy si� za 24h.\nPo tym czasie b�dziesz m�g� sprawdzi� szczeg�y stanu cia�a oraz opinie koronera.");
				}
				else
				{
					SendGuiInformation(playerid, "Informacja", "TEST");
					
					mysql_free_result();
				}
			}
		}

		case ITEM_TYPE_CD:
		{
			if( Item[itemid][item_value1] == 0 )
			{
				// Tworzymy plytke ;d
				
				pInfo[playerid][player_dialog_tmp1] = itemid;
				ShowPlayerDialog(playerid, DIALOG_CD_URL, DIALOG_STYLE_INPUT, "Tworzenie p�yty � Adres radia", "W poni�szym polu podaj adres do internetowego radia: (ogg/vorbis;mp3;.pls)", "Dalej", "Anuluj");
			}
			else
			{
				if( GetPlayerState(playerid) != PLAYER_STATE_DRIVER ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Aby u�y� p�yty musisz by� kierowc� pojazdu.");
				
				new vehicleid = GetPlayerVehicleID(playerid);
				foreach(new itid : Items)
				{
					if( Item[itid][item_owner_type] == ITEM_OWNER_TYPE_VEHICLE_COMPONENT && Item[itid][item_owner] == Vehicle[vehicleid][vehicle_uid] && Item[itid][item_type] == ITEM_TYPE_CD )
					{
						return SendGuiInformation(playerid, "Wyst�pi� b��d", "W radiu tego samochodu znajduje si� ju� inna p�yta. Aby j� wyci�gn�� u�yj komendy /v.");
					}
				}
				
				Item[itemid][item_owner_type] = ITEM_OWNER_TYPE_VEHICLE_COMPONENT;
				Item[itemid][item_owner] = Vehicle[vehicleid][vehicle_uid];
				
				mysql_query(sprintf("UPDATE crp_items SET item_ownertype = %d, item_owner = %d WHERE item_uid = %d", Item[itemid][item_owner_type], Item[itemid][item_owner], Item[itemid][item_uid]));
				
				ProxMessage(playerid, "wk�ada p�yt� do radia.", PROX_ME);
				
				new url[120];
				mysql_query(sprintf("SELECT audio_url FROM crp_audiourls WHERE audio_uid = %d", Item[itemid][item_value1]));
				mysql_store_result();
				
				mysql_fetch_field("audio_url", url);
				
				mysql_free_result();
				
				Vehicle[vehicleid][vehicle_radio] = true;
				
				foreach(new p : Player)
				{
					if( GetPlayerVehicleID(p) == vehicleid )
					{
						PlayAudioStreamForPlayer(p, url);
					}
				}
			}
		}

		case ITEM_TYPE_CUFFS:
		{
			if( !Item[itemid][item_used] )
			{
				// Jesli przedmiot nie jest aktywny
				if( GetPlayerUsedItem(playerid, ITEM_TYPE_CUFFS) != -1 ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Masz ju� u�yte inne kajdanki.");

				pInfo[playerid][player_dialog_tmp1] = itemid;

				DynamicGui_Init(playerid);
				new string[2048], count;
				
				new Float:p_pos[3];
				GetPlayerPos(playerid, p_pos[0], p_pos[1], p_pos[2]);
				
				foreach(new p : Player)
				{
					if( !pInfo[p][player_logged] ) continue;
					if( p == playerid ) continue;
					if( pInfo[p][player_admin_spec] ) continue;
					if( GetPlayerDistanceFromPoint(p, p_pos[0], p_pos[1], p_pos[2]) <= 5.0 )
					{
						//if( GetPlayerUsedItem(playerid, ITEM_TYPE_MASK) > -1 ) format(string, sizeof(string), "%s##\t\t%s\n", string, pInfo[p][player_name]);
						//else format(string, sizeof(string), "%s%d\t\t%s\n", string, p, pInfo[p][player_name]);
						
						format(string, sizeof(string), "%s%d\t\t%s\n", string, p, pInfo[p][player_name]);
						
						DynamicGui_AddRow(playerid, p);
						count++;
					}
				}
				
				if( count == 0 ) SendGuiInformation(playerid, "Wyst�pi� b��d", "W pobli�u nie ma �adnych os�b.");
				else ShowPlayerDialog(playerid, DIALOG_HANDCUFFS_SELECT, DIALOG_STYLE_LIST, "Osoby znajduj�ce si� w pobli�u:", string, "Wy�lij", "Zamknij");
			}
			else
			{
				// jesli jest aktywny
				new targetid = Item[itemid][item_value1];

				pInfo[targetid][player_is_cuffed] = false;
				pInfo[targetid][player_cuff_targetid] = INVALID_PLAYER_ID;

				RemovePlayerAttachedObject(targetid, pInfo[targetid][player_cuff_oindex]);
				pInfo[targetid][player_cuff_oindex] = -1;

				SetPlayerSpecialAction(targetid, SPECIAL_ACTION_NONE);

				Item[itemid][item_used] = false;

				GameTextForPlayer(playerid, "~g~Odkules ~w~gracza", 3000, 3);
				GameTextForPlayer(targetid, "~w~Zostales ~g~odkuty", 3000, 3);
			}
		}

		case ITEM_TYPE_DRUGS:
		{
			OnPlayerUseDrugs(playerid, itemid);
		}
	}
	
	return 1;
}

stock GetItemByData(i_type, i_value1 = -1, i_value2 = -1)
{
	new item = -1, bool:item_good = false;
	foreach(new itemid : Items)
	{
		item_good = true;
		if( Item[itemid][item_type] == i_type )
		{
			if( i_value1 > -1 )
			{
				if( Item[itemid][item_value1] != i_value1 ) item_good = false;
			}
			
			if( i_value2 > -1 )
			{
				if( Item[itemid][item_value2] != i_value2 ) item_good = false;
			}
		}
		else item_good = false;
		
		if( item_good )
		{
			item = itemid;
			break;
		}
	}
	
	return item;
}

stock GetPlayerUsedItem(playerid, i_type)
{
	foreach(new itemid : Items)
	{
		if( Item[itemid][item_owner_type] == ITEM_OWNER_TYPE_PLAYER && Item[itemid][item_owner] == pInfo[playerid][player_id] && Item[itemid][item_type] == i_type && Item[itemid][item_used] ) return itemid;
	}
	
	return -1;
}

stock GetPlayerUsedItemCount(playerid, i_type)
{
	new i = 0;
	
	foreach(new itemid : Items)
	{
		if( Item[itemid][item_owner_type] == ITEM_OWNER_TYPE_PLAYER && Item[itemid][item_owner] == pInfo[playerid][player_id] && Item[itemid][item_type] == i_type && Item[itemid][item_used] ) i++;
	}
	
	return i;
}

stock GetPlayerItem(playerid, i_type)
{
	foreach(new itemid : Items)
	{
		if( Item[itemid][item_owner_type] == ITEM_OWNER_TYPE_PLAYER && Item[itemid][item_owner] == pInfo[playerid][player_id] && Item[itemid][item_type] == i_type ) return itemid;
	}
	
	return -1;
}


/* native Item_Drop(itemid, playerid = INVALID_PLAYER_ID)
 * @params integer, integer
 * @returns true
 */
stock Item_Drop(itemid, playerid = INVALID_PLAYER_ID)
{
	if( playerid != INVALID_PLAYER_ID && (Item[itemid][item_owner_type] != ITEM_OWNER_TYPE_PLAYER || Item[itemid][item_owner] != pInfo[playerid][player_id]) ) return 1;
	
	if( Item[itemid][item_owner_type] == ITEM_OWNER_TYPE_GROUND )
	{
		Item[itemid][item_object] = CreateDynamicObject(Item[itemid][item_model], Item[itemid][item_x], Item[itemid][item_y], Item[itemid][item_z], Item[itemid][item_rx], Item[itemid][item_ry], Item[itemid][item_rz], Item[itemid][item_world], Item[itemid][item_interior], -1, 100.0);
	}
	else if( Item[itemid][item_owner_type] == ITEM_OWNER_TYPE_PLAYER )
	{
		if( Item[itemid][item_used] ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie mo�esz od�o�y� przedmiotu, kt�ry jest u�ywany");
		
		if( !IsPlayerInAnyVehicle(playerid) )
		{
			mysql_query(sprintf("SELECT * FROM `crp_items_proto` WHERE `model` = %d", Item[itemid][item_model]));
			mysql_store_result();
			
			if( mysql_num_rows() )
			{
				if( playerid != INVALID_PLAYER_ID )
				{
					GetPointInAngleOfPlayer(playerid, Item[itemid][item_x], Item[itemid][item_y], Item[itemid][item_z], 1.0, 0.0);
					
					Item[itemid][item_owner_type] = ITEM_OWNER_TYPE_GROUND;
					Item[itemid][item_owner] = 0;
					Item[itemid][item_z] = floatadd(Item[itemid][item_z], mysql_fetch_field_float("z"));
					Item[itemid][item_rx] = mysql_fetch_field_float("rx");
					Item[itemid][item_ry] = mysql_fetch_field_float("ry");
					Item[itemid][item_rz] = mysql_fetch_field_float("rz");
					Item[itemid][item_world] = GetPlayerVirtualWorld(playerid);
					Item[itemid][item_interior] = GetPlayerInterior(playerid);
					mysql_free_result();
					
					Item[itemid][item_object] = CreateDynamicObject(Item[itemid][item_model], Item[itemid][item_x], Item[itemid][item_y], Item[itemid][item_z], Item[itemid][item_rx], Item[itemid][item_ry], Item[itemid][item_rz], Item[itemid][item_world], Item[itemid][item_interior], -1, 100.0);
					Streamer_UpdateEx(playerid, Item[itemid][item_x], Item[itemid][item_y], Item[itemid][item_z]);
					ApplyAnimation(playerid, "BOMBER", "BOM_Plant", 4.0, 0, 0, 0, 0, 0, 1);
					
					new str[400];
					strcat(str, sprintf("UPDATE `crp_items` SET `item_ownertype` = %d, `item_owner` = 0, `item_posx` = %f, `item_posy` = %f, `item_posz` = %f,", Item[itemid][item_owner_type], Item[itemid][item_x], Item[itemid][item_y], Item[itemid][item_z]));
					strcat(str, sprintf(" `item_rotx` = %f, `item_roty` = %f, `item_rotz` = %f, `item_world` = %d, `item_interior` = %d  WHERE `item_uid` = %d", Item[itemid][item_rx], Item[itemid][item_ry], Item[itemid][item_rz], Item[itemid][item_world], Item[itemid][item_interior], Item[itemid][item_uid]));
					mysql_query(str);
					
					ProxMessage(playerid, "odk�ada co� na ziemi�.", PROX_ME);
				}
			}
			else
			{
				mysql_free_result();
				
				if( playerid != INVALID_PLAYER_ID )
				{
					if( !HasCrewFlag(playerid, CREW_FLAG_ITEMS) ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Podczas odk�adania przedmiotu wyst�pi� b��d.");
					
					GetPointInAngleOfPlayer(playerid, Item[itemid][item_x], Item[itemid][item_y], Item[itemid][item_z], 1.0, 0.0);
					
					Item[itemid][item_object] = CreateDynamicObject(Item[itemid][item_model], Item[itemid][item_x], Item[itemid][item_y], Item[itemid][item_z], Item[itemid][item_rx], Item[itemid][item_ry], Item[itemid][item_rz], Item[itemid][item_world], Item[itemid][item_interior], -1, 100.0);
					
					Streamer_UpdateEx(playerid, Item[itemid][item_x], Item[itemid][item_y], Item[itemid][item_z]);

					pInfo[playerid][player_items_proto_create] = true;
					pInfo[playerid][player_items_proto_create_id] = itemid;
					
					defer DelayEditObject[250](playerid, Item[itemid][item_object]);
					
					Item[itemid][item_owner_type] = ITEM_OWNER_TYPE_GROUND;
					Item[itemid][item_owner] = 0;
					
					SendPlayerInformation(playerid, "Ten model ~r~nie posiada wzoru~w~ przedmiotu w bazie danych, ~y~utworz~w~ go teraz.", 4000);
				}
			}
		}
		else
		{
			new vid = GetPlayerVehicleID(playerid);
			if( !CanPlayerUseVehicle(playerid, vid) ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Nie masz uprawnie� do odk�adania przedmiot�w w tym poje�dzie.");
			
			Item[itemid][item_owner_type] = ITEM_OWNER_TYPE_VEHICLE;
			Item[itemid][item_owner] = Vehicle[vid][vehicle_uid];
			
			mysql_query(sprintf("UPDATE `crp_items` SET `item_ownertype` = %d, `item_owner` = %d WHERE `item_uid` = %d", Item[itemid][item_owner_type], Item[itemid][item_owner], Item[itemid][item_uid]));
			ApplyAnimation(playerid, "BOMBER", "BOM_Plant", 4.0, 0, 0, 0, 0, 0, 1);
			
			ProxMessage(playerid, "odk�ada co� do pojazdu.", PROX_ME);
		}
	}
	
	return 1;
}

/* native Item_Pickup(itemid, playerid = INVALID_PLAYER_ID)
 * @params integer, integer
 * @returns true
 */
stock Item_Pickup(itemid, playerid = INVALID_PLAYER_ID)
{
	if( playerid != INVALID_PLAYER_ID && Item[itemid][item_owner_type] == ITEM_OWNER_TYPE_PLAYER ) return 1;
	
	if( Item[itemid][item_owner_type] == ITEM_OWNER_TYPE_GROUND )
	{
		DestroyDynamicObject(Item[itemid][item_object]);
		
		Item[itemid][item_object] = -1;
		
		Item[itemid][item_x] = 0.0;
		Item[itemid][item_y] = 0.0;
		Item[itemid][item_z] = 0.0;
		Item[itemid][item_rx] = 0.0;
		Item[itemid][item_ry] = 0.0;
		Item[itemid][item_rz] = 0.0;
		Item[itemid][item_world] = 0;
		Item[itemid][item_interior] = 0;
		
		ProxMessage(playerid, sprintf("podnosi %s z ziemi.", Item[itemid][item_name]), PROX_ME);
	}
	else if( Item[itemid][item_owner_type] == ITEM_OWNER_TYPE_VEHICLE )
	{
		ProxMessage(playerid, sprintf("podnosi %s z pojazdu.", Item[itemid][item_name]), PROX_ME);
	}
	else if( Item[itemid][item_owner_type] == ITEM_OWNER_TYPE_DOOR )
	{
		ProxMessage(playerid, sprintf("wyci�ga %s ze schowka.", Item[itemid][item_name]), PROX_ME);
	}
	
	Item[itemid][item_owner_type] = ITEM_OWNER_TYPE_PLAYER;
	Item[itemid][item_owner] = pInfo[playerid][player_id];
	
	ApplyAnimation(playerid, "BOMBER", "BOM_Plant", 4.0, 0, 0, 0, 0, 0, 1);
	
	mysql_query(sprintf("UPDATE `crp_items` SET `item_ownertype` = %d, `item_owner` = %d, `item_posx` = 0.0, `item_posy` = 0.0, `item_posz` = 0.0, `item_rotx` = 0.0, `item_roty` = 0.0, `item_rotz` = 0.0, `item_world` = 0, `item_interior` = 0  WHERE `item_uid` = %d", Item[itemid][item_owner_type], Item[itemid][item_owner], Item[itemid][item_uid]));
	
	return 1;
}

/* native Item_Create(owner_type, owner_id, i_type, i_model, i_val1, i_val2, i_name[40])
 * @params integer, integer, integer, integer, integer, integer, string[40]
 * @returns true
 */
stock Item_Create(owner_type, owner_id, i_type, i_model, i_val1, i_val2, i_name[], i_amount = 0, i_price = 0, i_group = 0)
{
	new itemid = -1;

	if( owner_type == ITEM_OWNER_TYPE_PLAYER )
	{
		new playerid = owner_id;
		if( owner_id > -1 )
		{
			if(i_type == ITEM_TYPE_DRUG_INGR && i_amount == 0) i_amount = 1;

			mysql_query(sprintf("INSERT INTO `crp_items` (`item_uid`,`item_model`,`item_ownertype`,`item_owner`,`item_type`,`item_value1`,`item_value2`,`item_name`,`item_created`,`item_amount`,`item_price`,`item_group`) VALUES (null, %d, %d, %d, %d, %d, %d,'%s', %d, %d, %d, %d)", i_model, owner_type, pInfo[playerid][player_id], i_type, i_val1, i_val2, i_name, gettime(), i_amount, i_price, i_group));
			new uid = mysql_insert_id();
			
			itemid = LoadItem(sprintf("WHERE `item_uid` = %d", uid), true);
		}
	}
	else if( owner_type == ITEM_OWNER_TYPE_PACKAGE )
	{
		mysql_query(sprintf("INSERT INTO `crp_items` (`item_uid`,`item_model`,`item_ownertype`,`item_owner`,`item_type`,`item_value1`,`item_value2`,`item_name`,`item_created`,`item_amount`,`item_price`) VALUES (null, %d, %d, %d, %d, %d, %d,'%s', %d, %d, %d)", i_model, owner_type, owner_id, i_type, i_val1, i_val2, i_name, gettime(), i_amount, i_price));
		new uid = mysql_insert_id();
		
		itemid = LoadItem(sprintf("WHERE `item_uid` = %d", uid), true);
	}
	else if( owner_type == ITEM_OWNER_TYPE_DOOR_WAREHOUSE )
	{
		mysql_query(sprintf("INSERT INTO `crp_items` (`item_uid`,`item_model`,`item_ownertype`,`item_owner`,`item_type`,`item_value1`,`item_value2`,`item_name`,`item_created`,`item_amount`,`item_price`) VALUES (null, %d, %d, %d, %d, %d, %d,'%s', %d, %d, %d)", i_model, owner_type, owner_id, i_type, i_val1, i_val2, i_name, gettime(), i_amount, i_price));
		new uid = mysql_insert_id();
		
		itemid = LoadItem(sprintf("WHERE `item_uid` = %d", uid), true);
	}
	return itemid;
}

stock GetItemByUid(itemuid)
{
	foreach(new item : Items)
	{
		if( Item[item][item_uid] == itemuid ) return item;
	}
	
	return -1;
}