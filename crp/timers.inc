timer UnfreezePlayer[1000](playerid, freeze_id)
{
	if( pInfo[playerid][player_freeze_id] == freeze_id )
	{
		TogglePlayerControllable(playerid, 1);
		pInfo[playerid][player_freeze_id] = 0;
	}
}

timer HideInformationTextdraw[100](playerid, information_id)
{
	if( pInfo[playerid][player_info_td_id] == information_id )
	{
		PlayerTextDrawHide(playerid, pInfo[playerid][informationTd]);
		pInfo[playerid][player_info_td_id] = 0;
	}
}

timer DelayEditObject[500](playerid, objectid)
{
	EditDynamicObject(playerid, objectid);
}

timer LockVehicleWithDelay[500](vid)
{
	Vehicle[vid][vehicle_locked] = true;
	Vehicle[vid][vehicle_state] = 0;
}

timer SetVehicleBound[500](vid, Float:x, Float:y, Float:z, Float:a)
{
	SetVehiclePos(vid, x, y, z);
	SetVehicleZAngle(vid, a);
	
	SetVehicleVelocity(vid, 0.0, 0.0, 0.0);
}

timer StopPlayerHurted[1000](playerid)
{
	if( pInfo[playerid][player_hurted] ) 
	{
		pInfo[playerid][player_hurted] = false;
		ApplyAnimation(playerid, "CARRY", "crry_prtial", 4.0, 0, 0, 0, 0, 0, 1);
		
		RemovePlayerStatus(playerid, PLAYER_STATUS_HURT);
	}
}

timer ApplyAnim[1000](playerid, type)
{
	switch( type )
	{
		case ANIM_TYPE_BW: ApplyAnimation(playerid, "KNIFE", "KILL_Knife_Ped_Die", 4.0, 0, 0, 0, 1, 0, 1);
		case ANIM_TYPE_BW_INCAR: ApplyAnimation(playerid, "ped", "CAR_dead_LHS", 4.0, 0, 0, 0, 1, 0, 1);

	}
}

timer HideGroupDutyPane[100](playerid)
{
	PlayerTextDrawHide(playerid, pInfo[playerid][DashboardPane][0]);
	PlayerTextDrawHide(playerid, pInfo[playerid][DashboardPane][1]);
}

timer LoadAttachedObjects[1000](playerid)
{
	foreach(new itemid : Items)
	{
		if( Item[itemid][item_owner_type] == ITEM_OWNER_TYPE_PLAYER && Item[itemid][item_owner] == pInfo[playerid][player_id] && Item[itemid][item_type] == ITEM_TYPE_ACCESSORY && Item[itemid][item_used] )
		{
			new freeid = GetPlayerFreeAttachSlot(playerid);
			if( freeid == -1 ) break;
			
			// zakladamy
			mysql_query(sprintf("SELECT * FROM crp_access WHERE access_uid = %d", Item[itemid][item_value1]));
			mysql_store_result();
			
			SetPlayerAttachedObject(playerid, freeid, mysql_fetch_field_int("access_model"), mysql_fetch_field_int("access_bone"), mysql_fetch_field_float("access_posx"), mysql_fetch_field_float("access_posy"), mysql_fetch_field_float("access_posz"), 
			mysql_fetch_field_float("access_rotx"), mysql_fetch_field_float("access_roty"), mysql_fetch_field_float("access_rotz"), mysql_fetch_field_float("access_scalex"), mysql_fetch_field_float("access_scaley"), mysql_fetch_field_float("access_scalez"));
			
			Item[itemid][item_value2] = freeid;
			
			mysql_free_result();
		}
	}
}

timer VehicleEngineStart[1000](playerid, vehicleid)
{
	TextDrawHideForPlayer(playerid, vehicleEngineStarting);
	TextDrawHideForPlayer(playerid, vehicleInfo);
	
	Vehicle[vehicleid][vehicle_engine_starting] = false;
	Vehicle[vehicleid][vehicle_engine] = true;
	
	SendPlayerInformation(playerid, "Silnik pojazdu zostal ~g~odpalony~w~!", 3500);
	
	UpdateVehicleVisuals(vehicleid);
}

timer BuggedHotel[500](playerid)
{
	GameTextForPlayer(playerid, "~p~aby wyjsc z pokoju~n~~y~lalt + space", 8000, 3);
}

timer VehicleStateErase[1000](vid)
{
	Vehicle[vid][vehicle_state] = 0;
	Vehicle[vid][vehicle_state_end] = 0;
	Vehicle[vid][vehicle_state_time] = 0;
	
	UpdateDynamic3DTextLabelText(Vehicle[vid][vehicle_state_label], COLOR_WHITE, "");
}

timer StopPlayerAnimation[200](playerid)
{
	ApplyAnimation(playerid, "CARRY", "crry_prtial", 4.0, 0, 0, 0, 0, 0, 1);
}

timer HideFuelTextdraw[200](playerid)
{
	PlayerTextDrawHide(playerid, pInfo[playerid][vehicleFuelInfo]);
}

timer BusSelectPrepare[5](playerid)
{
	SetPlayerFacingAngle(playerid, 0);
	
	SetPlayerCameraPos(playerid, pInfo[playerid][player_bus_camera][0]-0.5, pInfo[playerid][player_bus_camera][1], pInfo[playerid][player_bus_camera][2]+80.0);	
	SetPlayerCameraLookAt(playerid, pInfo[playerid][player_bus_camera][0], pInfo[playerid][player_bus_camera][1], pInfo[playerid][player_bus_camera][2]);
	
	PlayerTextDrawSetString(playerid, pInfo[playerid][BusInfo], "Za pomoca strzalek zmieniaj pozycje kamery, aby odnalezc miejsce podrozy.~n~~n~Wcisnij ~y~~k~~VEHICLE_ENTER_EXIT~ ~w~aby zobaczyc najblizszy przystanek lub ~y~~k~~PED_JUMPING~ ~w~aby anulowac.");
	PlayerTextDrawShow(playerid, pInfo[playerid][BusInfo]);
}

timer DrugCooking[100](cooking_uid, objectid, start, end)
{

	new Float:percent = floatdiv((gettime()-start),(end-start))*100;

	if(percent >= 100)
	{
		UpdateDynamic3DTextLabelText(Object[objectid][object_label], COLOR_WHITE, "{00FF00}Stol alchemiczny\n"HEX_COLOR_WHITE"Gotowanie zako�czone\n (( u�yj /craft aby zgarn�� towar ))");
		Object[objectid][object_is_drug_cooked] = false;
		Object[objectid][object_drug_cooked_id] = cooking_uid;
	}
	else UpdateDynamic3DTextLabelText(Object[objectid][object_label], COLOR_WHITE, sprintf("{00FF00}Stol alchemiczny\n"HEX_COLOR_WHITE"Gotowanie\n%d%%", floatround(percent)));

	if(percent < 100) {
		defer DrugCooking[1000](cooking_uid, objectid, start, end);
	}
}

timer PreloadAllAnimLibs[200](playerid)
{
	PreloadAnimLib(playerid,"BOMBER");
   	PreloadAnimLib(playerid,"RAPPING");
    PreloadAnimLib(playerid,"SHOP");
   	PreloadAnimLib(playerid,"BEACH");
   	PreloadAnimLib(playerid,"SMOKING");
	PreloadAnimLib(playerid,"RYDER");
	PreloadAnimLib(playerid,"PLAYIDLES");
	PreloadAnimLib(playerid,"POOL");
	PreloadAnimLib(playerid,"DANCING");
	PreloadAnimLib(playerid,"LOWRIDER");
	PreloadAnimLib(playerid,"INT_SHOP");
	PreloadAnimLib(playerid,"wuzi");
	PreloadAnimLib(playerid,"CRIB");
	PreloadAnimLib(playerid,"POLICE");
	PreloadAnimLib(playerid,"GRAVEYARD");
	PreloadAnimLib(playerid,"FIGHT_D");
    PreloadAnimLib(playerid,"ON_LOOKERS");
	PreloadAnimLib(playerid,"RIOT");
	PreloadAnimLib(playerid,"GANGS");
    PreloadAnimLib(playerid,"DEALER");
	PreloadAnimLib(playerid,"VENDING");
	PreloadAnimLib(playerid,"HEIST9");
	PreloadAnimLib(playerid,"CRACK");
	PreloadAnimLib(playerid,"SPRAYCAN");
	PreloadAnimLib(playerid,"JST_BUISNESS");
	PreloadAnimLib(playerid,"PAULNMAC");
	PreloadAnimLib(playerid,"GRENADE");
	PreloadAnimLib(playerid,"WAYFARER");
	PreloadAnimLib(playerid,"INT_OFFICE");
	PreloadAnimLib(playerid,"ROB_BANK");
	PreloadAnimLib(playerid,"STRIP");
	PreloadAnimLib(playerid,"GHANDS");
	PreloadAnimLib(playerid,"PARK");
	PreloadAnimLib(playerid,"CAR");
	PreloadAnimLib(playerid,"CARRY");
	PreloadAnimLib(playerid,"KISSING");
	PreloadAnimLib(playerid,"MISC");
	PreloadAnimLib(playerid,"OTB");
	PreloadAnimLib(playerid,"COP_AMBIENT");
	PreloadAnimLib(playerid,"MEDIC");
	PreloadAnimLib(playerid,"INT_HOUSE");
	PreloadAnimLib(playerid,"FOOD");
	PreloadAnimLib(playerid,"PED");
	PreloadAnimLib(playerid,"SWEET");
	PreloadAnimLib(playerid,"KNIFE");
	PreloadAnimLib(playerid,"CASINO");
	PreloadAnimLib(playerid,"AIRPORT");
	PreloadAnimLib(playerid,"Attractors");
	PreloadAnimLib(playerid,"BAR");
	PreloadAnimLib(playerid,"BASEBALL");
	PreloadAnimLib(playerid,"BD_FIRE");
	PreloadAnimLib(playerid,"benchpress");
	PreloadAnimLib(playerid,"BF_injection");
	PreloadAnimLib(playerid,"BLOWJOBZ");
	PreloadAnimLib(playerid,"BOX");
	PreloadAnimLib(playerid,"BSKTBALL");
	PreloadAnimLib(playerid,"BUDDY");
	PreloadAnimLib(playerid,"CAMERA");
	PreloadAnimLib(playerid,"DAM_JUMP");
	PreloadAnimLib(playerid,"FAT");
}