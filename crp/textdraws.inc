stock CreatePlayerTextdraws(playerid)
{	
	pInfo[playerid][informationTd] = CreatePlayerTextDraw(playerid, 459.000000, 283.000000, " ");
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][informationTd], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][informationTd], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][informationTd], 0.170000, 1.100000);
	PlayerTextDrawColor(playerid, pInfo[playerid][informationTd], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][informationTd], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][informationTd], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][informationTd], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][informationTd], 128);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][informationTd], 606.000000, 30.000000);

	pInfo[playerid][gymTd] = CreatePlayerTextDraw(playerid, 490.000000, 294.000000, " ");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][gymTd], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][gymTd], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][gymTd], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][gymTd], 0.180000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][gymTd], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][gymTd], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][gymTd], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][gymTd], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][gymTd], 153);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][gymTd], 0.000000, 134.000000);
	
	pInfo[playerid][Dashboard] = CreatePlayerTextDraw(playerid, 320.000000, 300.000000, " ");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][Dashboard], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][Dashboard], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][Dashboard], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][Dashboard], 0.290000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][Dashboard], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][Dashboard], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][Dashboard], 1);
	
	pInfo[playerid][DoorInfo][0] = CreatePlayerTextDraw(playerid, 470.000000, 341.000000, "~n~");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][DoorInfo][0], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][DoorInfo][0], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][DoorInfo][0], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][DoorInfo][0], 0.500000, 6.900000);
	PlayerTextDrawColor(playerid, pInfo[playerid][DoorInfo][0], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][DoorInfo][0], 0);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][DoorInfo][0], 1);
	PlayerTextDrawSetShadow(playerid, pInfo[playerid][DoorInfo][0], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][DoorInfo][0], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][DoorInfo][0], 112);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][DoorInfo][0], 0.000000, 170.000000);

	pInfo[playerid][DoorInfo][1] = CreatePlayerTextDraw(playerid, 466.000000, 346.000000, "Los Santos Police Department~n~~n~~w~Wejscie: ~g~$5");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][DoorInfo][1], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][DoorInfo][1], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][DoorInfo][1], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][DoorInfo][1], 0.230000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][DoorInfo][1], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][DoorInfo][1], 0);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][DoorInfo][1], 1);
	PlayerTextDrawSetShadow(playerid, pInfo[playerid][DoorInfo][1], 1);

	pInfo[playerid][DoorInfo][2] = CreatePlayerTextDraw(playerid, 470.000000, 384.000000, "~y~Aby wejsc, wcisnij jednoczesnie~n~~w~[~b~~h~LALT + SPACE~w~]");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][DoorInfo][2], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][DoorInfo][2], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][DoorInfo][2], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][DoorInfo][2], 0.230000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][DoorInfo][2], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][DoorInfo][2], 0);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][DoorInfo][2], 1);
	PlayerTextDrawSetShadow(playerid, pInfo[playerid][DoorInfo][2], 1);
	
	pInfo[playerid][AreaInfo] = CreatePlayerTextDraw(playerid, 548.000000, 48.000000, "#area: ~y~432");
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][AreaInfo], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][AreaInfo], 3);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][AreaInfo], 0.290000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][AreaInfo], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][AreaInfo], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][AreaInfo], 1);
	
	pInfo[playerid][leftTime] = CreatePlayerTextDraw(playerid, 440.000000, 320.000000, " ");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][leftTime], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][leftTime], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][leftTime], 3);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][leftTime], 0.610000, 2.400000);
	PlayerTextDrawColor(playerid, pInfo[playerid][leftTime], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][leftTime], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][leftTime], 1);
	
	CreateGroupPlayerTextdraws(playerid);
	
	pInfo[playerid][GroupDutyTag] = CreatePlayerTextDraw(playerid, 8.000000, 428.000000, "GOV");
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][GroupDutyTag], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][GroupDutyTag], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][GroupDutyTag], 0.230000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][GroupDutyTag], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][GroupDutyTag], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][GroupDutyTag], 1);
		
	pInfo[playerid][DashboardPane][0] = CreatePlayerTextDraw(playerid, 202.000000, 340.000000, "~n~");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][DashboardPane][0], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][DashboardPane][0], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][DashboardPane][0], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][DashboardPane][0], 0.500000, 14.000000);
	PlayerTextDrawColor(playerid, pInfo[playerid][DashboardPane][0], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][DashboardPane][0], 0);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][DashboardPane][0], 1);
	PlayerTextDrawSetShadow(playerid, pInfo[playerid][DashboardPane][0], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][DashboardPane][0], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][DashboardPane][0], 0x9E9EAD20);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][DashboardPane][0], 730.000000, -950.000000);

	pInfo[playerid][DashboardPane][1] = CreatePlayerTextDraw(playerid, 152.000000, 349.000000, "Sluzba: ~b~grupa                    ~y~Ganton Gym (UID: 249)~n~~n~~w~Rozpoczeto: ~g~14:24               ~w~Tytul: ~r~Lider");
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][DashboardPane][1], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][DashboardPane][1], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][DashboardPane][1], 0.290000, 1.000000);
	PlayerTextDrawColor(playerid, pInfo[playerid][DashboardPane][1], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][DashboardPane][1], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][DashboardPane][1], 1);
	
	pInfo[playerid][vehicleFuelInfo] = CreatePlayerTextDraw(playerid, 244.000000, 330.000000, "~y~PALIWO: ~g~51/57 L");
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][vehicleFuelInfo], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][vehicleFuelInfo], 2);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][vehicleFuelInfo], 0.479999, 1.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][vehicleFuelInfo], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][vehicleFuelInfo], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][vehicleFuelInfo], 1);
	
	pInfo[playerid][OfferTD][0] = CreatePlayerTextDraw(playerid, 190.000000, 180.000000, "~n~ ~y~UID: ~w~456~n~~n~ ~y~Model: ~w~ 411~n~~n~~n~~n~~n~~n~~n~~n~");
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][OfferTD][0], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][OfferTD][0], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][OfferTD][0], 0.230000, 0.799998);
	PlayerTextDrawColor(playerid, pInfo[playerid][OfferTD][0], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][OfferTD][0], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][OfferTD][0], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][OfferTD][0], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][OfferTD][0], 51);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][OfferTD][0], 440.000000, 0.000000);

	pInfo[playerid][OfferTD][1] = CreatePlayerTextDraw(playerid, 345.000000, 188.000000, "~n~~n~~n~~n~~n~~n~~n~~n~");
	PlayerTextDrawFont(playerid, pInfo[playerid][OfferTD][1], TEXT_DRAW_FONT_MODEL_PREVIEW);
 	PlayerTextDrawUseBox(playerid, pInfo[playerid][OfferTD][1], 1);
    PlayerTextDrawBoxColor(playerid, pInfo[playerid][OfferTD][1], 0x00000051);
    PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][OfferTD][1], 0x00000051);
    PlayerTextDrawTextSize(playerid, pInfo[playerid][OfferTD][1], 80.0, 40.0);

	pInfo[playerid][OfferTD][2] = CreatePlayerTextDraw(playerid, 190.000000, 250.000000, "~y~~h~ Oferta od Vincent Dabrasco ~>~ vCard~n~~n~~b~~h~ Nazwa:~w~ bleh~n~~g~ Koszt:~w~ $0~n~~n~~n~~n~");
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][OfferTD][2], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][OfferTD][2], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][OfferTD][2], 0.250000, 0.799998);
	PlayerTextDrawColor(playerid, pInfo[playerid][OfferTD][2], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][OfferTD][2], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][OfferTD][2], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][OfferTD][2], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][OfferTD][2], 51);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][OfferTD][2], 440.000000, 0.000000);

	pInfo[playerid][OfferTD][3] = CreatePlayerTextDraw(playerid, 219.000000, 291.000000, "Szczegoly");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][OfferTD][3], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][OfferTD][3], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][OfferTD][3], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][OfferTD][3], 0.230000, 0.799998);
	PlayerTextDrawColor(playerid, pInfo[playerid][OfferTD][3], 0xFFDF00FF);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][OfferTD][3], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][OfferTD][3], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][OfferTD][3], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][OfferTD][3], 51);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][OfferTD][3], 10.000000, 36.000000);
	PlayerTextDrawSetSelectable(playerid, pInfo[playerid][OfferTD][3], 1);

	pInfo[playerid][OfferTD][4] = CreatePlayerTextDraw(playerid, 345.000000, 291.000000, "Akceptuj");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][OfferTD][4], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][OfferTD][4], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][OfferTD][4], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][OfferTD][4], 0.230000, 0.799998);
	PlayerTextDrawColor(playerid, pInfo[playerid][OfferTD][4], 0x009300FF);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][OfferTD][4], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][OfferTD][4], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][OfferTD][4], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][OfferTD][4], 51);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][OfferTD][4], 10.000000, 36.000000);
	PlayerTextDrawSetSelectable(playerid, pInfo[playerid][OfferTD][4], 1);

	pInfo[playerid][OfferTD][5] = CreatePlayerTextDraw(playerid, 403.000000, 291.000000, "Odrzuc");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][OfferTD][5], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][OfferTD][5], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][OfferTD][5], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][OfferTD][5], 0.230000, 0.799998);
	PlayerTextDrawColor(playerid, pInfo[playerid][OfferTD][5], 0xC60000FF);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][OfferTD][5], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][OfferTD][5], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][OfferTD][5], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][OfferTD][5], 51);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][OfferTD][5], 10.000000, 36.000000);
	PlayerTextDrawSetSelectable(playerid, pInfo[playerid][OfferTD][5], 1);
	
	pInfo[playerid][BusInfo] = CreatePlayerTextDraw(playerid, 312.000000, 330.000000, " ");
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][BusInfo], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][BusInfo], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][BusInfo], 0.230000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][BusInfo], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][BusInfo], 0);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][BusInfo], 1);
	PlayerTextDrawSetShadow(playerid, pInfo[playerid][BusInfo], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][BusInfo], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][BusInfo], 96);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][BusInfo], 611.000000, 0.000000);

	pInfo[playerid][CarColorPicker][0] = CreatePlayerTextDraw(playerid, 246.000000, 351.000000, "Poprzedni kolor");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][CarColorPicker][0], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][CarColorPicker][0], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][CarColorPicker][0], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][CarColorPicker][0], 0.180000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][CarColorPicker][0], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][CarColorPicker][0], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][CarColorPicker][0], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][CarColorPicker][0], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][CarColorPicker][0], 96);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][CarColorPicker][0], 10.000000, 71.000000);
	PlayerTextDrawSetSelectable(playerid, pInfo[playerid][CarColorPicker][0], true);

	pInfo[playerid][CarColorPicker][1] = CreatePlayerTextDraw(playerid, 389.000000, 351.000000, "Nastepny kolor");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][CarColorPicker][1], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][CarColorPicker][1], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][CarColorPicker][1], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][CarColorPicker][1], 0.180000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][CarColorPicker][1], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][CarColorPicker][1], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][CarColorPicker][1], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][CarColorPicker][1], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][CarColorPicker][1], 96);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][CarColorPicker][1], 10.000000, 71.000000);
	PlayerTextDrawSetSelectable(playerid, pInfo[playerid][CarColorPicker][1], true);

	pInfo[playerid][CarColorPicker][2] = CreatePlayerTextDraw(playerid, 317.000000, 347.000000, "~b~~h~~h~135~n~~w~Aktualny");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][CarColorPicker][2], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][CarColorPicker][2], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][CarColorPicker][2], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][CarColorPicker][2], 0.180000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][CarColorPicker][2], -16776961);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][CarColorPicker][2], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][CarColorPicker][2], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][CarColorPicker][2], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][CarColorPicker][2], 96);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][CarColorPicker][2], 20.000000, 47.000000);
	PlayerTextDrawSetSelectable(playerid, pInfo[playerid][CarColorPicker][2], true);

	pInfo[playerid][CarColorPicker][3] = CreatePlayerTextDraw(playerid, 317.000000, 370.000000, "Wybierz");
	PlayerTextDrawAlignment(playerid, pInfo[playerid][CarColorPicker][3], 2);
	PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][CarColorPicker][3], 255);
	PlayerTextDrawFont(playerid, pInfo[playerid][CarColorPicker][3], 1);
	PlayerTextDrawLetterSize(playerid, pInfo[playerid][CarColorPicker][3], 0.180000, 0.899999);
	PlayerTextDrawColor(playerid, pInfo[playerid][CarColorPicker][3], -1);
	PlayerTextDrawSetOutline(playerid, pInfo[playerid][CarColorPicker][3], 1);
	PlayerTextDrawSetProportional(playerid, pInfo[playerid][CarColorPicker][3], 1);
	PlayerTextDrawUseBox(playerid, pInfo[playerid][CarColorPicker][3], 1);
	PlayerTextDrawBoxColor(playerid, pInfo[playerid][CarColorPicker][3], 96);
	PlayerTextDrawTextSize(playerid, pInfo[playerid][CarColorPicker][3], 10.000000, 47.000000);
	PlayerTextDrawSetSelectable(playerid, pInfo[playerid][CarColorPicker][3], true);
}

stock CreateTextdraws()
{
	BlackScreen = TextDrawCreate(700.000000, -31.000000, "~n~");
	TextDrawLetterSize(BlackScreen, 0.500000, 57.000000);
	TextDrawUseBox(BlackScreen, 1);
	TextDrawBoxColor(BlackScreen, 255);
	TextDrawTextSize(BlackScreen, -70.000000, 0.000000);

	CRPversion = TextDrawCreate(498.000000, 3.000000, "istory roleplay~n~~w~ver. 0.1");
	TextDrawBackgroundColor(CRPversion, 255);
	TextDrawFont(CRPversion, 3);
	TextDrawLetterSize(CRPversion, 0.390000, 0.899999);
	TextDrawColor(CRPversion, -1717986817);
	TextDrawSetOutline(CRPversion, 1);
	TextDrawSetProportional(CRPversion, 1);
	
	PlayerVip = TextDrawCreate(532.000000, 420.000000, "Gracz Premium");
	TextDrawBackgroundColor(PlayerVip, -154914561);
	TextDrawFont(PlayerVip, 2);
	TextDrawLetterSize(PlayerVip, 0.259999, 0.899999);
	TextDrawColor(PlayerVip, -1);
	TextDrawSetOutline(PlayerVip, 1);
	TextDrawSetProportional(PlayerVip, 1);
	
	vehicleInfo = TextDrawCreate(438.000000, 366.000000, "Aby uruchomic silnik, wcisnij ~y~LCTRL ~w~+ ~y~LALT~w~.~n~Klawisz ~y~LMB ~w~kontroluje swiatla w pojezdzie.");
	TextDrawBackgroundColor(vehicleInfo, 255);
	TextDrawFont(vehicleInfo, 1);
	TextDrawLetterSize(vehicleInfo, 0.220000, 0.899999);
	TextDrawColor(vehicleInfo, -1);
	TextDrawSetOutline(vehicleInfo, 0);
	TextDrawSetProportional(vehicleInfo, 1);
	TextDrawSetShadow(vehicleInfo, 1);
	TextDrawUseBox(vehicleInfo, 1);
	TextDrawBoxColor(vehicleInfo, 128);
	TextDrawTextSize(vehicleInfo, 611.000000, 0.000000);
	
	vehicleEngineStarting = TextDrawCreate(325.000000, 328.000000, "trwa odpalanie silnika...");
 	TextDrawAlignment(vehicleEngineStarting, 2);
 	TextDrawBackgroundColor(vehicleEngineStarting, 255);
 	TextDrawFont(vehicleEngineStarting, 2);
 	TextDrawLetterSize(vehicleEngineStarting, 0.479999, 1.799998);
 	TextDrawColor(vehicleEngineStarting, -1);
 	TextDrawSetOutline(vehicleEngineStarting, 1);
 	TextDrawSetProportional(vehicleEngineStarting, 1);
 	TextDrawSetShadow(vehicleEngineStarting, 1);
	
	CreateGroupStaticTextdraws();
	
	// PENALTIES
	PenaltiesTextDraw[0] = TextDrawCreate(104.000000, 251.000000, "Ban (365)");
	TextDrawAlignment(PenaltiesTextDraw[0], 2);
	TextDrawBackgroundColor(PenaltiesTextDraw[0], 153);
	TextDrawFont(PenaltiesTextDraw[0], 1);
	TextDrawLetterSize(PenaltiesTextDraw[0], 0.219999, 0.799998);
	TextDrawColor(PenaltiesTextDraw[0], -1027424001);
	TextDrawSetOutline(PenaltiesTextDraw[0], 0);
	TextDrawSetProportional(PenaltiesTextDraw[0], 1);
	TextDrawSetShadow(PenaltiesTextDraw[0], 1);
	TextDrawUseBox(PenaltiesTextDraw[0], 1);
	TextDrawBoxColor(PenaltiesTextDraw[0], 136);
	TextDrawTextSize(PenaltiesTextDraw[0], 189.000000, 170.000000);

	PenaltiesTextDraw[1] = TextDrawCreate(19.000000, 261.000000, "Nadawca: Kostek~n~Odbiorca: Wivek~n~~n~Powod:~w~~n~Homoseksualista");
	TextDrawBackgroundColor(PenaltiesTextDraw[1], 153);
	TextDrawFont(PenaltiesTextDraw[1], 1);
	TextDrawLetterSize(PenaltiesTextDraw[1], 0.219999, 0.799998);
	TextDrawColor(PenaltiesTextDraw[1], -1);
	TextDrawSetOutline(PenaltiesTextDraw[1], 0);
	TextDrawSetProportional(PenaltiesTextDraw[1], 1);
	TextDrawSetShadow(PenaltiesTextDraw[1], 1);
	TextDrawUseBox(PenaltiesTextDraw[1], 1);
	TextDrawBoxColor(PenaltiesTextDraw[1], 85);
	TextDrawTextSize(PenaltiesTextDraw[1], 189.000000, 170.000000);
	// PENALTIES
		
	LSNtd = TextDrawCreate(1.000000, 439.000000, "~y~~h~LSN ~>~ ~w~Brak sygnalu nadawania.");
	TextDrawBackgroundColor(LSNtd, 153);
	TextDrawFont(LSNtd, 1);
	TextDrawLetterSize(LSNtd, 0.209998, 0.799997);
	TextDrawColor(LSNtd, -1);
	TextDrawSetOutline(LSNtd, 1);
	TextDrawSetProportional(LSNtd, 1);
	TextDrawUseBox(LSNtd, 1);
	TextDrawBoxColor(LSNtd, 85);
	TextDrawTextSize(LSNtd, 720.000000, 170.000000);
	
	ZegarekTD = TextDrawCreate(575.000000, 49.000000, "12:11");
	TextDrawBackgroundColor(ZegarekTD, 255);
	TextDrawFont(ZegarekTD, 3);
	TextDrawLetterSize(ZegarekTD, 0.340000, 1.100000);
	TextDrawColor(ZegarekTD, -1);
	TextDrawSetOutline(ZegarekTD, 1);
	TextDrawSetProportional(ZegarekTD, 1);
}

stock CreateGroupPlayerTextdraws(playerid)
{
	new Float:baseY = 183.0;
	for(new i=0;i<5;i++)
	{
		pInfo[playerid][GroupsListRow][i] = CreatePlayerTextDraw(playerid, 104.000000, baseY+(i*14), "1       Pig Pen (669)");
		PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][GroupsListRow][i], 255);
		PlayerTextDrawFont(playerid, pInfo[playerid][GroupsListRow][i], 1);
		PlayerTextDrawLetterSize(playerid, pInfo[playerid][GroupsListRow][i], 0.290000, 1.000000);
		PlayerTextDrawColor(playerid, pInfo[playerid][GroupsListRow][i], -1);
		PlayerTextDrawSetOutline(playerid, pInfo[playerid][GroupsListRow][i], 1);
		PlayerTextDrawSetProportional(playerid, pInfo[playerid][GroupsListRow][i], 1);
		PlayerTextDrawUseBox(playerid, pInfo[playerid][GroupsListRow][i], 1);
		PlayerTextDrawBoxColor(playerid, pInfo[playerid][GroupsListRow][i], 112);
		PlayerTextDrawTextSize(playerid, pInfo[playerid][GroupsListRow][i], 537.000000, 0.000000);
	}
	
	baseY = 184.0;
	for(new i=0;i<5;i++)
	{
		pInfo[playerid][GroupsListStaticButtons][i*5] = CreatePlayerTextDraw(playerid, 344.000000, baseY+(playerid, i*14), "Info");
		PlayerTextDrawAlignment(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], 2);
		PlayerTextDrawBackgroundColor(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], 255);
		PlayerTextDrawFont(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], 1);
		PlayerTextDrawLetterSize(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], 0.190000, 0.799999);
		PlayerTextDrawColor(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], -1);
		PlayerTextDrawSetOutline(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], 1);
		PlayerTextDrawSetProportional(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], 1);
		PlayerTextDrawUseBox(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], 1);
		PlayerTextDrawBoxColor(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], 112);
		PlayerTextDrawTextSize(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], 12.0, 36.0);
		PlayerTextDrawSetSelectable(playerid,  pInfo[playerid][GroupsListStaticButtons][i*5], 1);

		pInfo[playerid][GroupsListStaticButtons][(i*5)+1] = CreatePlayerTextDraw(playerid, 387.000000, baseY+(playerid, i*14), "Pojazdy");
		PlayerTextDrawAlignment(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], 2);
		PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], 255);
		PlayerTextDrawFont(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], 1);
		PlayerTextDrawLetterSize(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], 0.190000, 0.799999);
		PlayerTextDrawColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], -1);
		PlayerTextDrawSetOutline(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], 1);
		PlayerTextDrawSetProportional(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], 1);
		PlayerTextDrawUseBox(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], 1);
		PlayerTextDrawBoxColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], 112);
		PlayerTextDrawTextSize(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], 10.0, 36.0);
		PlayerTextDrawSetSelectable(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+1], 1);

		pInfo[playerid][GroupsListStaticButtons][(i*5)+2] = CreatePlayerTextDraw(playerid, 430.000000, baseY+(playerid, i*14), "Sluzba");
		PlayerTextDrawAlignment(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], 2);
		PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], 255);
		PlayerTextDrawFont(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], 1);
		PlayerTextDrawLetterSize(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], 0.190000, 0.799999);
		PlayerTextDrawColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], -1);
		PlayerTextDrawSetOutline(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], 1);
		PlayerTextDrawSetProportional(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], 1);
		PlayerTextDrawUseBox(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], 1);
		PlayerTextDrawBoxColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], 112);
		PlayerTextDrawTextSize(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], 10.0, 36.0);
		PlayerTextDrawSetSelectable(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+2], 1);

		pInfo[playerid][GroupsListStaticButtons][(i*5)+3] = CreatePlayerTextDraw(playerid, 473.000000, baseY+(playerid, i*14), "Magazyn");
		PlayerTextDrawAlignment(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], 2);
		PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], 255);
		PlayerTextDrawFont(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], 1);
		PlayerTextDrawLetterSize(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], 0.190000, 0.799999);
		PlayerTextDrawColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], -1);
		PlayerTextDrawSetOutline(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], 1);
		PlayerTextDrawSetProportional(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], 1);
		PlayerTextDrawUseBox(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], 1);
		PlayerTextDrawBoxColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], 112);
		PlayerTextDrawTextSize(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], 10.0, 36.0);
		PlayerTextDrawSetSelectable(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+3], 1);

		pInfo[playerid][GroupsListStaticButtons][(i*5)+4] = CreatePlayerTextDraw(playerid, 516.000000, baseY+(playerid, i*14), "Online");
		PlayerTextDrawAlignment(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], 2);
		PlayerTextDrawBackgroundColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], 255);
		PlayerTextDrawFont(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], 1);
		PlayerTextDrawLetterSize(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], 0.190000, 0.799999);
		PlayerTextDrawColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], -1);
		PlayerTextDrawSetOutline(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], 1);
		PlayerTextDrawSetProportional(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], 1);
		PlayerTextDrawUseBox(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], 1);
		PlayerTextDrawBoxColor(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], 112);
		PlayerTextDrawTextSize(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], 10.0, 36.0);
		PlayerTextDrawSetSelectable(playerid, pInfo[playerid][GroupsListStaticButtons][(i*5)+4], 1);
	}
}

stock CreateGroupStaticTextdraws()
{
	GroupsListStaticHeader = TextDrawCreate(104.000000, 172.000000, "~y~SLOT           NAZWA GRUPY                                          OPCJE");
	TextDrawBackgroundColor(GroupsListStaticHeader, 255);
	TextDrawFont(GroupsListStaticHeader, 2);
	TextDrawLetterSize(GroupsListStaticHeader, 0.280000, 0.799999);
	TextDrawColor(GroupsListStaticHeader, -1);
	TextDrawSetOutline(GroupsListStaticHeader, 1);
	TextDrawSetProportional(GroupsListStaticHeader, 1);
}

stock DestroyPlayerTextdraws(playerid)
{
	for(new i=0;i<5;i++)
	{
		PlayerTextDrawDestroy(playerid, pInfo[playerid][GroupsListRow][i]);
	}
	
	for(new y=0;y<25;y++)
	{
		PlayerTextDrawDestroy(playerid, pInfo[playerid][GroupsListStaticButtons][y]);
	}
	
	PlayerTextDrawDestroy(playerid, pInfo[playerid][gymTd]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][informationTd]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][Dashboard]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][DoorInfo][0]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][DoorInfo][1]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][DoorInfo][2]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][AreaInfo]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][leftTime]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][GroupDutyTag]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][DashboardPane][0]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][DashboardPane][1]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][vehicleFuelInfo]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][OfferTD][0]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][OfferTD][1]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][OfferTD][2]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][OfferTD][3]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][OfferTD][4]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][OfferTD][5]);
	PlayerTextDrawDestroy(playerid, pInfo[playerid][BusInfo]);
}