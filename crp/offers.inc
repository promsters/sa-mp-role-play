stock SetOffer(sellerid, buyerid, type, price, extraid = -1)
{
	if( sellerid == buyerid && !HasCrewFlag(sellerid, CREW_FLAG_ADMIN_ROOT) )
	{
		SendGuiInformation(sellerid, "Wyst�pi� b��d", "Nie mo�esz wysy�a� ofert samemu sobie.");
		return 0;
	}
	
	/*if( GetPlayerOnlineTime(buyerid) < 18000 && sellerid != INVALID_PLAYER_ID && price > 0 )
	{
		SendGuiInformation(sellerid, "Wyst�pi� b��d", "Ten gracz ma przegrane mniej ni� 5h.");
		return 0;
	}*/

	if( sellerid != INVALID_PLAYER_ID )
	{
		if( pOffer[sellerid][offer_type] > 0 )
		{
			SendGuiInformation(sellerid, "Wyst�pi� b��d", "Nie mo�esz sk�ada� kilku ofert na raz, poczekaj do zako�czenia poprzedniej oferty.");
			return 0;
		}
		
		/*if( GetPlayerOnlineTime(sellerid) < 18000 && price > 0 )
		{
			SendGuiInformation(sellerid, "Wyst�pi� b��d", "Nie mo�esz sk�ada� ofert poniewa� masz przegrane mniej ni� 5h.");
			return 0;
		}*/
	}
	
	if( pOffer[buyerid][offer_type] > 0 )
	{
		SendGuiInformation(sellerid, "Wyst�pi� b��d", "Gracz, do kt�rego chcia�e� wys�a� ofert� aktualnie otrzyma� ju� jedn�.");
		return 0;
	}
	
	if( pInfo[buyerid][player_afk] )
	{
		SendGuiInformation(sellerid, "Wyst�pi� b��d", "Gracz, do kt�rego chcia�e� wys�a� ofert� aktualnie jest AFK.");
		return 0;
	}
	
	switch( type )
	{
		case OFFER_TYPE_VCARD:
		{
			new itemid = GetPlayerUsedItem(buyerid, ITEM_TYPE_PHONE);
			if( GetPlayerUsedItem(buyerid, ITEM_TYPE_PHONE) == -1 )
			{
				SendGuiInformation(sellerid, "Wyst�pi� b��d", "Gracz, do kt�rego chcia�e� wys�a� ofert� nie posiada w��czonego telefonu.");
				return 0;
			}
			
			mysql_query(sprintf("SELECT contact_uid FROM `crp_contacts` WHERE `contact_owner` = %d AND `contact_number` = %d AND `contact_deleted` = 0", Item[itemid][item_uid], Item[extraid][item_value1]));
			mysql_store_result();
			
			if( mysql_num_rows() == 1 )
			{
				SendGuiInformation(sellerid, "Wyst�pi� b��d", "Gracz, do kt�rego chcia�e� wys�a� ofert� posiada ju� Twoj� wizyt�wk� w telefonie.");
				mysql_free_result();
				return 0;
			}
			mysql_free_result();
		}

		case OFFER_TYPE_VEH_SELL: 
		{
			PlayerTextDrawSetPreviewModel(buyerid, pInfo[buyerid][OfferTD][1], Vehicle[extraid][vehicle_model]);
			PlayerTextDrawSetPreviewVehCol(buyerid, pInfo[buyerid][OfferTD][1], Vehicle[extraid][vehicle_color][0], Vehicle[extraid][vehicle_color][1]);
			PlayerTextDrawSetPreviewRot(buyerid, pInfo[buyerid][OfferTD][1], 345.0, 0.0, 310.0, 0.9);
		 	PlayerTextDrawSetString(buyerid, pInfo[buyerid][OfferTD][0], sprintf("~n~ ~y~UID: ~w~%d~n~ ~y~Model: ~w~ %d~n~ ~y~Rodzaj paliwa: ~w~%s~n~ ~y~Stan techniczny: ~w~%.1f HP~n~ ~y~Przebieg: ~w~%d km~n~~n~", Vehicle[extraid][vehicle_uid], Vehicle[extraid][vehicle_model], VehicleFuelTypes[Vehicle[extraid][vehicle_fuel_type]], Vehicle[extraid][vehicle_health], floatround(Vehicle[extraid][vehicle_mileage], floatround_floor)));
		}
	}
	
	pOffer[buyerid][offer_type] = type;
	if( sellerid == INVALID_PLAYER_ID ) pOffer[buyerid][offer_sellerid] = -1;
	else pOffer[buyerid][offer_sellerid] = sellerid;
	
	pOffer[buyerid][offer_buyerid] = INVALID_PLAYER_ID;
	pOffer[buyerid][offer_price] = price;
	pOffer[buyerid][offer_extraid] = extraid;
	
	if( sellerid != INVALID_PLAYER_ID )
	{
		pOffer[sellerid][offer_type] = type;
		pOffer[sellerid][offer_sellerid] = INVALID_PLAYER_ID;
		pOffer[sellerid][offer_buyerid] = buyerid;
		pOffer[sellerid][offer_price] = price;
		pOffer[sellerid][offer_extraid] = extraid;
		
		pOffer[sellerid][offer_start] = gettime();
	}
	pOffer[buyerid][offer_start] = gettime();
	
	return 1;
}

stock ShowPlayerOffer(playerid, fromid, header[], name[], price, bool:show_details = false)
{
	new tmp[MAX_PLAYER_NAME+1];
	if( fromid != INVALID_PLAYER_ID )
	{
		strcopy(tmp, pInfo[fromid][player_name], MAX_PLAYER_NAME+1);
	}
	else strcopy(tmp, "System");
	PlayerTextDrawSetString(playerid, pInfo[playerid][OfferTD][2], sprintf("~y~~h~ Oferta od %s ~>~ %s~n~~n~~b~~h~ Nazwa:~w~ %s~n~~g~ Koszt:~w~ $%d~n~~n~~n~~n~", tmp, header, name, price));
	
	HidePlayerDialog(playerid);
	
	PlayerTextDrawShow(playerid, pInfo[playerid][OfferTD][2]);
	PlayerTextDrawShow(playerid, pInfo[playerid][OfferTD][4]);
	PlayerTextDrawShow(playerid, pInfo[playerid][OfferTD][5]);
	
	if( show_details ) {
		PlayerTextDrawShow(playerid, pInfo[playerid][OfferTD][0]);
		PlayerTextDrawShow(playerid, pInfo[playerid][OfferTD][1]);
	}
	
	SelectTextDraw(playerid, COLOR_WHITE);
	
	if( fromid != INVALID_PLAYER_ID ) SendClientMessage(fromid, COLOR_LIGHTER_ORANGE, "Oferta zosta�a wys�ana. Odczekaj chwil�, by przekona� si�, czy gracz zaakceptuje Twoj� ofert�.");
	
	return 1;
}

stock OnPlayerOfferResponse(playerid, response)
{
	for(new i;i<6;i++) PlayerTextDrawHide(playerid, pInfo[playerid][OfferTD][i]);
	CancelSelectTextDraw(playerid);
	
	new sellerid = pOffer[playerid][offer_sellerid];
	
	if( !response )
	{
		if( sellerid != INVALID_PLAYER_ID ) GameTextForPlayer(sellerid, "~w~oferta ~r~odrzucona", 3000, 3);
		
		OnPlayerOfferRejected(playerid, pOffer[playerid][offer_type]);
		
		for(new x=0; e_player_offer:x != e_player_offer; x++)
		{
			pOffer[playerid][e_player_offer:x] = 0;
			if( sellerid != INVALID_PLAYER_ID ) pOffer[sellerid][e_player_offer:x] = 0;
		}
	}
	else
	{
		pOffer[playerid][offer_accepted] = true;
		if( pOffer[playerid][offer_price] == 0 ) OnPlayerOfferFinish(playerid, 0, 1);
		else
		{
			ShowPlayerPayment(playerid);
		}
	}
	return 1;
}

stock OnPlayerOfferRejected(buyerid, type)
{
	switch( type )
	{
		case OFFER_TYPE_VEH_SELL:
		{
			new vid = pOffer[buyerid][offer_extraid];
			Vehicle[vid][vehicle_state] = 0;
		}
	}
}

stock OnPlayerOfferFinish(playerid, type, response)
{
	new sellerid = pOffer[playerid][offer_sellerid];
	
	if( !response )
	{
		if( sellerid != INVALID_PLAYER_ID ) GameTextForPlayer(sellerid, "~w~oferta ~r~odrzucona", 3000, 3);
		OnPlayerOfferRejected(playerid, pOffer[playerid][offer_type]);
	}
	else
	{
		if( sellerid != INVALID_PLAYER_ID ) 
		{			
			GameTextForPlayer(sellerid, "~w~oferta ~g~zaakceptowana", 3000, 3);
		}
		
		switch( pOffer[playerid][offer_type] )
		{
			case OFFER_TYPE_VCARD:
			{
				new itemid = GetPlayerUsedItem(playerid, ITEM_TYPE_PHONE);
				if( itemid > -1 )
				{
					mysql_query(sprintf("INSERT INTO `crp_contacts` VALUES (null, %d, '%s', %d, 0)", Item[pOffer[playerid][offer_extraid]][item_value1], pInfo[sellerid][player_name], Item[itemid][item_uid]));
					ProxMessage(sellerid, sprintf("wys�a� wizyt�wk� do %s", pInfo[playerid][player_name]), PROX_ME);
				}	
			}
			
			case OFFER_TYPE_DOCUMENT:
			{
				pInfo[playerid][player_documents] += pOffer[playerid][offer_extraid];
				mysql_query(sprintf("UPDATE `crp_characters` SET `char_documents` = %d WHERE `char_uid` = %d", pInfo[playerid][player_documents], pInfo[playerid][player_id]));
				
				ProxMessage(playerid, "odbiera od Cerpka dokument.", PROX_ME);
			}
			
			case OFFER_TYPE_VEH_SELL:
			{
				if( type == 1 ) AddPlayerBankMoney(sellerid, pOffer[playerid][offer_price]);
				else GivePlayerMoney(sellerid, pOffer[playerid][offer_price]);
				
				new vid = pOffer[playerid][offer_extraid];
				mysql_query(sprintf("UPDATE `crp_vehicles` SET `vehicle_ownertype` = %d, `vehicle_owner` = %d WHERE `vehicle_uid` = %d", VEHICLE_OWNER_TYPE_PLAYER, pInfo[playerid][player_id], Vehicle[vid][vehicle_uid]));
				Vehicle[vid][vehicle_owner_type] = VEHICLE_OWNER_TYPE_PLAYER;
				Vehicle[vid][vehicle_owner] = pInfo[playerid][player_id];
				Vehicle[vid][vehicle_locked] = false;
				Vehicle[vid][vehicle_engine] = false;
				UpdateVehicleVisuals(vid);
				
				foreach(new p : Player)
				{
					if( GetPlayerVehicleID(p) == vid ) RemovePlayerFromVehicle(p);
				}

				SendGuiInformation(playerid, "Informacja", sprintf("Zakupi�e� pojazd %s. Jest on teraz dost�pny pod /v. Nie zapomnij go jak najszybciej zaparkowa�!", VehicleNames[Vehicle[vid][vehicle_model]-400]));
			}
			
			case OFFER_TYPE_LSN_REKLAMA:
			{
				GiveGroupMoney(pOffer[playerid][offer_extraid], pOffer[playerid][offer_price]);
				GiveGroupPoints(pOffer[playerid][offer_extraid], 3);
			}
			
			case OFFER_TYPE_LSN_WYWIAD:
			{
				pInfo[playerid][player_lsn_wywiad_starter] = sellerid;
				pInfo[playerid][player_lsn_wywiad_with] = INVALID_PLAYER_ID;
				
				pInfo[sellerid][player_lsn_wywiad_starter] = INVALID_PLAYER_ID;
				pInfo[sellerid][player_lsn_wywiad_with] = playerid;
			}
			
			case OFFER_TYPE_PETROL:
			{
				new vid = GetPlayerVehicleID(playerid);
				
				Vehicle[vid][vehicle_state] = VEHICLE_STATE_FUELING;
				Vehicle[vid][vehicle_state_end] = gettime() + floatround(1.5 * pOffer[playerid][offer_extraid], floatround_ceil);
				Vehicle[vid][vehicle_state_time] = Vehicle[vid][vehicle_state_end] - gettime();
				UpdateDynamic3DTextLabelText(Vehicle[vid][vehicle_state_label], COLOR_PINK, "Tankowanie pojazdu\n0%");
				
				Vehicle[vid][vehicle_fuel_current] = Vehicle[vid][vehicle_fuel_current] + float(pOffer[playerid][offer_extraid]);
				
				if( Vehicle[vid][vehicle_fuel_current] > float(VehicleFuelMax[GetVehicleModel(vid)-400]) ) Vehicle[vid][vehicle_fuel_current] = VehicleFuelMax[GetVehicleModel(vid)-400];
				
				SaveVehicle(vid);
			}
			
			case OFFER_TYPE_ITEM:
			{
				new itemid = pOffer[playerid][offer_extraid];
				
				if( Item[itemid][item_owner_type] != ITEM_OWNER_TYPE_PLAYER || Item[itemid][item_owner] != pInfo[sellerid][player_id] || Item[itemid][item_used] )
				{
					if( type == 1 ) AddPlayerBankMoney(playerid, pOffer[playerid][offer_price]);
					else GivePlayerMoney(playerid, pOffer[playerid][offer_price]);
					
					SendGuiInformation(playerid, "Wyst�pi� b��d", "Gracz, kt�ry zaoferowa� Ci ten przedmiot ju� go nie posiada.");
				}
				else
				{
					if( type == 1 ) AddPlayerBankMoney(sellerid, pOffer[playerid][offer_price]);
					else GivePlayerMoney(sellerid, pOffer[playerid][offer_price]);
					
					Item[itemid][item_owner] = pInfo[playerid][player_id];
					mysql_query(sprintf("UPDATE crp_items SET item_owner = %d WHERE item_uid = %d", Item[itemid][item_owner], Item[itemid][item_uid]));
					
					HidePlayerDialog(sellerid);
				}
			}
			
			case OFFER_TYPE_ITEM_PODAJ:
			{
				new itemid = pOffer[playerid][offer_extraid];
				if( Item[itemid][item_uid] <= 0 || Item[itemid][item_amount] <= 0 )
				{
					if( type == 1 ) AddPlayerBankMoney(playerid, pOffer[playerid][offer_price]);
					else GivePlayerMoney(playerid, pOffer[playerid][offer_price]);
					
					SendGuiInformation(playerid, "Wyst�pi� b��d", "Przedmiot, kt�ry zosta� Ci oferowany nie istnieje.");
				}
				else
				{
					new gid = GetGroupByUid(Door[GetDoorByUid(Item[itemid][item_owner])][door_owner]);
					GiveGroupPoints(gid, 3);
					GiveGroupMoney(gid, pOffer[playerid][offer_price]);
					
					new value1 = Item[itemid][item_value1];
					
					if( Item[itemid][item_type] == ITEM_TYPE_GYM_PASS ) value1 = Group[gid][group_uid];
					
					Item_Create(ITEM_OWNER_TYPE_PLAYER, playerid, Item[itemid][item_type], Item[itemid][item_model], value1, Item[itemid][item_value2], Item[itemid][item_name]);
					Item[itemid][item_amount] -= 1;
					
					if( Item[itemid][item_amount] > 0 ) mysql_query(sprintf("UPDATE crp_items SET item_amount = %d WHERE item_uid = %d", Item[itemid][item_amount], Item[itemid][item_uid]));
					else DeleteItem(itemid, true);
				}
				
			}
			
			case OFFER_TYPE_SALON_VEH:
			{
				new model = pOffer[playerid][offer_extraid];
				
				pInfo[playerid][player_dialog_tmp1] = model;

				if( GetVehicleType(INVALID_VEHICLE_ID, model) == VEHICLE_TYPE_BIKE || GetVehicleType(INVALID_VEHICLE_ID, model) == VEHICLE_TYPE_MOTORBIKE || GetVehicleType(INVALID_VEHICLE_ID, model) == VEHICLE_TYPE_TRAILER ) 
				{
					if( GetVehicleType(INVALID_VEHICLE_ID, model) == VEHICLE_TYPE_BIKE || GetVehicleType(INVALID_VEHICLE_ID, model) == VEHICLE_TYPE_TRAILER ) pInfo[playerid][player_dialog_tmp2] = -1;
					else pInfo[playerid][player_dialog_tmp2] = 1;
					
					ShowPlayerDialog(playerid, DIALOG_CARS_SHOP_COLOR, DIALOG_STYLE_INPUT, "Wyb�r koloru pojazdu", "W poni�sze pole wpisz kolor pojazdu w formacie kolor1:kolor2, np. 24:35.", "Wybierz", "Zamknij");
				}
				else ShowPlayerDialog(playerid, DIALOG_CARS_SHOP_FUELTYPE, DIALOG_STYLE_LIST, "Wyb�r rodzaju paliwa pojazdu", "Benzyna\nGaz\nDiesel", "Wybierz", "Zamknij");
			}
			
			case OFFER_TYPE_EMS_LECZENIE:
			{
				new gid = pOffer[playerid][offer_extraid];
				GiveGroupPoints(gid, 3);
				GiveGroupMoney(gid, pOffer[playerid][offer_price]);
				
				SetPlayerHealth(playerid, 100);
			}
			
			case OFFER_TYPE_YO:
			{
				new Float:gpos[4];
				GetPointInAngleOfPlayer(sellerid, gpos[0], gpos[1], gpos[2], 1.0, 0.0);
				
				SetPlayerPos(playerid, gpos[0], gpos[1], gpos[2]);
				SetPlayerFacingPlayer(sellerid, playerid);
				
				new animname[32];
				
				switch( pOffer[playerid][offer_extraid] )
				{
					case 1: strcopy(animname, "hndshkaa");
					case 2: strcopy(animname, "hndshkba");
					case 3: strcopy(animname, "hndshkca");
					case 4: strcopy(animname, "hndshkcb");
					case 5: strcopy(animname, "hndshkda");
					case 6: strcopy(animname, "hndshkea");
					case 7: strcopy(animname, "hndshkfa");
					case 8: strcopy(animname, "hndshkfa_swt");
				}
				
				ApplyAnimation(playerid, "GANGS", animname, 4.0, 0, 0, 0, 0, 0);
				ApplyAnimation(sellerid, "GANGS", animname, 4.0, 0, 0, 0, 0, 0);
			}
			
			case OFFER_TYPE_NAPRAWA:
			{			
				new vid = GetPlayerVehicleID(playerid);
				
				Vehicle[vid][vehicle_state] = VEHICLE_STATE_REPAIRING;
				Vehicle[vid][vehicle_state_time] = 30 + floatround((1000.0 - Vehicle[vid][vehicle_health]) / 3);
				Vehicle[vid][vehicle_state_end] = 0;
				Vehicle[vid][vehicle_state_repairing1] = sellerid;
				Vehicle[vid][vehicle_state_repairing2] = playerid;
				Vehicle[vid][vehicle_state_paytype] = type;
				
				UpdateDynamic3DTextLabelText(Vehicle[vid][vehicle_state_label], COLOR_PINK, "Naprawianie pojazdu\n0%");
				
				pInfo[sellerid][player_repairing] = true;
				pInfo[sellerid][player_repairing_veh] = vid;
				pInfo[sellerid][player_repairing_gid] = pOffer[playerid][offer_extraid];
				
				pInfo[playerid][player_repair_offer] = true;
				pInfo[playerid][player_repair_veh] = vid;
				pInfo[playerid][player_repair_price1] = pOffer[playerid][offer_price];

				SendGuiInformation(sellerid, "Informacja", "Rozpocz�to proces naprawiania pojazdu. W czasie jego trwania powiniene� odgrywa� akcj� role play za pomoc� /me i /do.\nInne zachowania b�d� karane.");
			}

			case OFFER_TYPE_MALOWANIE:
			{
				if( type == 1 ) AddPlayerBankMoney(playerid, -pOffer[playerid][offer_price]);
				else GivePlayerMoney(playerid, -pOffer[playerid][offer_price]);

				GiveGroupMoney(pOffer[sellerid][offer_extraid], pOffer[playerid][offer_price]);


				SendClientMessage(sellerid, COLOR_GOLD, "Oferta malowania pojazdu zosta�a zaakceptowana. Teraz zacznij malowa� pojazd, a� wska�nik post�pu nad pojazdem osi�gnie 100%.");
				SendClientMessage(playerid, COLOR_GOLD, "Tw�j pojazd b�dzie teraz przemalowywany. Post�p mo�esz �ledzi� patrz�c na wska�nik nad pojazdem.");
				new vid = GetVehicleByUid(pInfo[playerid][player_carpaint_vuid]);

				Vehicle[vid][vehicle_state] = VEHICLE_STATE_PAINT;
				Vehicle[vid][vehicle_state_time] = 2000;
				Vehicle[vid][vehicle_state_end] = gettime() + Vehicle[vid][vehicle_state_time];
				Vehicle[vid][vehicle_color][0] = pInfo[playerid][player_choosen_carcolor][0];
				Vehicle[vid][vehicle_color][1] = pInfo[playerid][player_choosen_carcolor][1];
				Vehicle[vid][vehicle_carpaint_seller] = pInfo[sellerid][player_id];
				Vehicle[vid][vehicle_carpaint_buyer] = pInfo[playerid][player_id];
			}
		}
	}
	
	for(new x=0; e_player_offer:x != e_player_offer; x++)
	{
		pOffer[playerid][e_player_offer:x] = 0;
		if( sellerid > -1 && sellerid != INVALID_PLAYER_ID ) pOffer[sellerid][e_player_offer:x] = 0;
	}
}