stock LoadDoor(limit[] = "", bool:return_id = false)
{
	mysql_query(sprintf("SELECT * FROM crp_doors %s", limit));  
	mysql_store_result();
	
	new rows = mysql_num_rows(), d_id;
	
	for(new i;i<rows;i++)
	{
		mysql_data_seek(i);
		mysql_fetch_row_data();

		d_id = CreateDynamicPickup(mysql_fetch_field_int("door_pickupid"), 1, mysql_fetch_field_float("door_enterx"), mysql_fetch_field_float("door_entery"), mysql_fetch_field_float("door_enterz"), mysql_fetch_field_int("door_entervw"), mysql_fetch_field_int("door_enterint"), -1, 200.0);
		
		Iter_Add(Doors, d_id);	
		
		mysql_fetch_field("door_name", Door[d_id][door_name]);
		mysql_fetch_field("door_audiourl", Door[d_id][door_audio]);
		
		Door[d_id][door_uid] = mysql_fetch_field_int("door_uid");
		Door[d_id][door_type] = mysql_fetch_field_int("door_type");
		Door[d_id][door_owner_type] = mysql_fetch_field_int("door_ownertype");
		Door[d_id][door_owner] = mysql_fetch_field_int("door_owner");

		Door[d_id][door_objects_limit] = mysql_fetch_field_int("door_objects");
		Door[d_id][door_auto_closing] = !!mysql_fetch_field_int("door_lock");
		Door[d_id][door_car_crosing] = !!mysql_fetch_field_int("door_garage");
		Door[d_id][door_payment] = mysql_fetch_field_int("door_enterpay");

		Door[d_id][door_pos][0] = mysql_fetch_field_float("door_enterx");
		Door[d_id][door_pos][1] = mysql_fetch_field_float("door_entery");
		Door[d_id][door_pos][2] = mysql_fetch_field_float("door_enterz");
		Door[d_id][door_pos][3] = mysql_fetch_field_float("door_entera");
		Door[d_id][door_vw] = mysql_fetch_field_int("door_entervw");
		Door[d_id][door_int] = mysql_fetch_field_int("door_enterint");
		Door[d_id][door_spawn_pos][0] = mysql_fetch_field_float("door_exitx");
		Door[d_id][door_spawn_pos][1] = mysql_fetch_field_float("door_exity");
		Door[d_id][door_spawn_pos][2] = mysql_fetch_field_float("door_exitz");
		Door[d_id][door_spawn_pos][3] = mysql_fetch_field_float("door_exita");
		Door[d_id][door_spawn_vw] = mysql_fetch_field_int("door_exitvw");
		if( Door[d_id][door_spawn_vw] == -1 ) Door[d_id][door_spawn_vw] = Door[d_id][door_uid];
		Door[d_id][door_spawn_int] = mysql_fetch_field_int("door_exitint");
		
		Door[d_id][door_value1] = mysql_fetch_field_int("door_value1");
		
		new map_model = mysql_fetch_field_int("door_mapmodel");
		if( map_model != -1 )
		{
			Door[d_id][door_map_icon] = CreateDynamicMapIcon(Door[d_id][door_pos][0], Door[d_id][door_pos][1], Door[d_id][door_pos][2], map_model, 0, Door[d_id][door_vw], Door[d_id][door_int]);
		}
			
		// Strefa zewn. drzwi
		Door[d_id][door_area_outer] = CreateDynamicSphere(Door[d_id][door_pos][0], Door[d_id][door_pos][1], Door[d_id][door_pos][2], 1.2, Door[d_id][door_vw], Door[d_id][door_int]);
		
		Area[Door[d_id][door_area_outer]][area_type] = AREA_TYPE_DOOR_OUTER;
		Area[Door[d_id][door_area_outer]][area_owner_type] = 0;
		Area[Door[d_id][door_area_outer]][area_owner] = d_id;
		Area[Door[d_id][door_area_outer]][area_uid] = -1;
		
		Iter_Add(Areas, Door[d_id][door_area_outer]);
		
		// Strefa wewn. drzwi
		Door[d_id][door_area_inner] = CreateDynamicSphere(Door[d_id][door_spawn_pos][0], Door[d_id][door_spawn_pos][1], Door[d_id][door_spawn_pos][2], 1.2, Door[d_id][door_spawn_vw], Door[d_id][door_spawn_int]);
		
		Area[Door[d_id][door_area_inner]][area_type] = AREA_TYPE_DOOR_INNER;
		Area[Door[d_id][door_area_inner]][area_owner_type] = 0;
		Area[Door[d_id][door_area_inner]][area_owner] = d_id;
		Area[Door[d_id][door_area_inner]][area_uid] = -1;
		
		Iter_Add(Areas, Door[d_id][door_area_inner]);
		
		Door[d_id][door_closed] = Door[d_id][door_auto_closing];
	}

	mysql_free_result();
	
	if( return_id ) return d_id;
	return rows;
}

stock DeleteDoor(d_id, bool:from_mysql = true)
{	
	if( from_mysql ) mysql_query(sprintf("DELETE FROM `crp_doors` WHERE `door_uid` = %d", Door[d_id][door_uid]));
	
	Iter_Remove(Doors, d_id);
	
	DestroyDynamicPickup(d_id);
	
	if( IsValidDynamicMapIcon(Door[d_id][door_map_icon]) ) DestroyDynamicMapIcon(Door[d_id][door_map_icon]);
	
	DestroyDynamicArea(Door[d_id][door_area_inner]);
	Iter_Remove(Areas, Door[d_id][door_area_inner]);
		
	DestroyDynamicArea(Door[d_id][door_area_outer]);
	Iter_Remove(Areas, Door[d_id][door_area_outer]);
	
	for(new z=0; e_doors:z != e_doors; z++)
    {
		Door[d_id][e_doors:z] = 0;
    }
}

stock GetDoorByUid(uid)
{
	foreach(new d_id : Doors)
	{
		if( Door[d_id][door_uid] == uid ) return d_id;
	}
	
	return -1;
}

stock CanPlayerUseDoor(playerid, d_id)
{
	if( HasCrewFlag(playerid, CREW_FLAG_DOORS) ) return 1;
	
	switch( Door[d_id][door_owner_type] )
	{
		case DOOR_OWNER_TYPE_GLOBAL:
		{
			if( Door[d_id][door_owner] == pInfo[playerid][player_id] )
			{
				new parent_did = GetDoorByUid(Door[d_id][door_vw]);
				if( parent_did > -1 )
				{
					if( Door[parent_did][door_owner_type] == DOOR_OWNER_TYPE_GROUP )
					{
						new gid = GetGroupByUid(Door[parent_did][door_owner]);
						if( gid > -1 )
						{
							if( Group[gid][group_type] == GROUP_TYPE_SOCIAL_HOUSE )
							{
								return 1;
							}
						}
					}
				}
			}
		}
		
		case DOOR_OWNER_TYPE_PLAYER:
		{
			if( Door[d_id][door_owner] == pInfo[playerid][player_id] ) return 1;
			if( pInfo[playerid][player_door] == Door[d_id][door_uid] ) return 1;
		}
		
		case DOOR_OWNER_TYPE_GROUP:
		{
			new gid = GetGroupByUid(Door[d_id][door_owner]);
			
			new slot = GetPlayerGroupSlot(playerid, gid);
			if( slot > -1 ) 
			{
				if( WorkerHasFlag(playerid, slot, WORKER_FLAG_DOORS) ) return 1;
			}
		}
	}
	
	return 0;
}

stock CanPlayerEditDoor(playerid, d_id)
{
	if(HasCrewFlag(playerid, CREW_FLAG_DOORS) ) return 1;

	switch( Door[d_id][door_owner_type] )
	{
		case DOOR_OWNER_TYPE_GLOBAL:
		{
			if( Door[d_id][door_owner] == pInfo[playerid][player_id] )
			{
				new parent_did = GetDoorByUid(Door[d_id][door_vw]);
				if( parent_did > -1 )
				{
					if( Door[parent_did][door_owner_type] == DOOR_OWNER_TYPE_GROUP )
					{
						new gid = GetGroupByUid(Door[parent_did][door_owner]);
						if( gid > -1 )
						{
							if( Group[gid][group_type] == GROUP_TYPE_SOCIAL_HOUSE )
							{
								return 1;
							}
						}
					}
				}
			}
		}
		
		case DOOR_OWNER_TYPE_PLAYER:
		{
			if( Door[d_id][door_owner] == pInfo[playerid][player_id] ) return 1;
		}
		
		case DOOR_OWNER_TYPE_GROUP:
		{
			new gid = GetGroupByUid(Door[d_id][door_owner]);
			
			new slot = GetPlayerGroupSlot(playerid, gid);
			if( slot > -1 ) 
			{
				if( WorkerHasFlag(playerid, slot, WORKER_FLAG_LEADER) ) return 1;
			}
		}
	}
	return 0;
}

stock CountDoorObjects(d_id)
{
	new count;
	foreach(new oid : Objects)
	{
		if( Object[oid][object_owner_type] == OBJECT_OWNER_TYPE_DOOR && Object[oid][object_owner] == Door[d_id][door_uid] ) count++;
	}
	
	return count;
}

stock CountDoorLabels(d_id)
{
	new count;
	foreach(new lid : Labels)
	{
		if( Label[Text3D:lid][label_owner_type] == LABEL_OWNER_TYPE_DOOR && Label[Text3D:lid][label_owner] == Door[d_id][door_uid] ) count++;
	}
	
	return count;
}

stock ShowPlayerDoorTextdraw(playerid, d_id)
{
	if( !HasCrewFlag(playerid, CREW_FLAG_DOORS) )
	{
		if( Door[d_id][door_payment] > 0 && !Door[d_id][door_closed] ) PlayerTextDrawSetString(playerid, pInfo[playerid][DoorInfo][1], sprintf("%s~n~~n~Wejscie: ~g~$%d", Door[d_id][door_name], Door[d_id][door_payment]));
		else if( Door[d_id][door_payment] == 0 ) PlayerTextDrawSetString(playerid, pInfo[playerid][DoorInfo][1], Door[d_id][door_name]);
	}
	else
	{
		if( Door[d_id][door_payment] > 0 && !Door[d_id][door_closed] ) PlayerTextDrawSetString(playerid, pInfo[playerid][DoorInfo][1], sprintf("%s (%d)~n~~n~Wejscie: ~g~$%d", Door[d_id][door_name], d_id, Door[d_id][door_payment]));
		else if( Door[d_id][door_payment] == 0 ) PlayerTextDrawSetString(playerid, pInfo[playerid][DoorInfo][1], sprintf("%s (%d)", Door[d_id][door_name], d_id));
	}
	
	if( Door[d_id][door_closed] ) PlayerTextDrawSetString(playerid, pInfo[playerid][DoorInfo][2], "~n~~w~[~r~~h~DRZWI ZAMKNIETE~w~]");
	else PlayerTextDrawSetString(playerid, pInfo[playerid][DoorInfo][2], "~y~Aby wejsc, wcisnij jednoczesnie~n~~w~[~b~~h~LALT + SPACE~w~]");

	for(new i;i<3;i++) PlayerTextDrawShow(playerid, pInfo[playerid][DoorInfo][i]);
}

stock HidePlayerDoorTextdraw(playerid)
{
	for(new i;i<3;i++) PlayerTextDrawHide(playerid, pInfo[playerid][DoorInfo][i]);
}

stock DoorsDefaultInteriorsList(playerid, d_id, page=1)
{
	mysql_query("SELECT COUNT(*) as count FROM `crp_default_interiors`");
	mysql_store_result();
	new all_rows = mysql_fetch_field_int("count");
	mysql_free_result();
	
	new header[64], Float:pp = all_rows / 20;
	format(header, sizeof(header), "Zmiana wn�trza drzwi (%d/%d)", page, floatround(pp, floatround_ceil));
	
	pInfo[playerid][player_dialog_tmp1] = page;
	
	DynamicGui_Init(playerid);
	DynamicGui_SetDialogValue(playerid, d_id);
	
	new str[800];
	
	if( page > 1 )
	{
		format(str, sizeof(str), "%s"HEX_COLOR_SAMP"<<< Poprzednia strona\n  \n", str);
		DynamicGui_AddRow(playerid, DG_DRZWI_CHANGE_INTERIOR_PREV);
		
		DynamicGui_AddBlankRow(playerid);
	}
	
	if( page == 1 )
	{
		format(str, sizeof(str), "%s0.\tBrak (w�asne obiekty)\n", str);
		DynamicGui_AddRow(playerid, DG_DRZWI_CHANGE_INTERIOR_ROW, -1);
	}
	
	mysql_query(sprintf("SELECT name, id FROM `crp_default_interiors` LIMIT %d, 20", (page-1)*20));
	mysql_store_result();
	
	new tmp_str[60];
	
	for(new i;i<mysql_num_rows();i++)
	{
		mysql_data_seek(i);
		mysql_fetch_row_data();
		
		mysql_fetch_field("name", tmp_str);
		
		format(str, sizeof(str), "%s%d.\t%s\n", str, (i+1)+((page-1)*20), tmp_str);
		DynamicGui_AddRow(playerid, DG_DRZWI_CHANGE_INTERIOR_ROW, mysql_fetch_field_int("id"));
	}
	
	mysql_free_result();
	
	if( all_rows > (page*20) )
	{
		format(str, sizeof(str), "%s"HEX_COLOR_SAMP"\t\t\t\t\t\t\t   Nastepna strona >>>\n", str);
		DynamicGui_AddRow(playerid, DG_DRZWI_CHANGE_INTERIOR_NEXT);
	}
	
	ShowPlayerDialog(playerid, DIALOG_ADRZWI_CHANGE_INTERIOR, DIALOG_STYLE_LIST, header, str, "Wybierz", "Wr��");
	
	return 1;
}
