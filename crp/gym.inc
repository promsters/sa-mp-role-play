stock GymStartBench(playerid, objectid)
{
	if( pInfo[playerid][player_is_gym_training] ) return SendGuiInformation(playerid, "Wyst�pi� b��d", "Rozpocz��e� ju� �wiczenie na si�owni.");

	new Float:pos[3], Float:rot;
	GetPointInAngleOfObject(objectid, pos[0], pos[1], pos[2], 1.0, 180.0);
	Streamer_GetFloatData(STREAMER_TYPE_OBJECT, objectid, E_STREAMER_R_Z, rot);
	TogglePlayerControllable(playerid, 0);

	SetPlayerPos(playerid, pos[0], pos[1], pos[2]+0.5);
	SetPlayerFacingAngle(playerid, rot);
	ApplyAnimation( playerid, "benchpress", "gym_bp_geton", 1, 0, 0, 0, 1, 0, 1 );

	GetPointInAngleOfObject(objectid, pos[0], pos[1], pos[2], 3.5, 170.0);
	SetPlayerCameraPos(playerid, pos[0], pos[1], pos[2]+3.0);


	Streamer_GetFloatData(STREAMER_TYPE_OBJECT, objectid, E_STREAMER_X, pos[0]);
	Streamer_GetFloatData(STREAMER_TYPE_OBJECT, objectid, E_STREAMER_Y, pos[1]);
	Streamer_GetFloatData(STREAMER_TYPE_OBJECT, objectid, E_STREAMER_Z, pos[2]);
	SetPlayerCameraLookAt(playerid, pos[0], pos[1], pos[2]+1.0);

	pInfo[playerid][player_is_gym_training] = true;
	pInfo[playerid][player_gym_object] = objectid;
	pInfo[playerid][player_gym_reps] = 0;

	defer BenchStart[3800](playerid);

	return 1;
}

stock GymStop(playerid)
{
	switch(Object[pInfo[playerid][player_gym_object]][object_type])
	{
		case OBJECT_TYPE_GYM_BENCH:
		{
			ApplyAnimation( playerid, "benchpress", "gym_bp_getoff", 1, 0, 0, 0, 1, 0, 1 );
			PlayerTextDrawHide(playerid, pInfo[playerid][gymTd]);

			pInfo[playerid][player_gym_can_workout] = false;
			defer BenchBeforeStop[2800](playerid);
			defer BenchStop[3800](playerid);
		}
	}
}

stock Gym_OnPlayerKey(playerid, newkeys, oldkeys)
{
	if(pInfo[playerid][player_is_gym_training])
	{
		if( PRESSED(KEY_HANDBRAKE) || PRESSED(KEY_FIRE) )
		{
			if( !pInfo[playerid][player_gym_can_workout] ) return;
			GymStop(playerid);
		}

		if( PRESSED(KEY_JUMP) )
		{
			if( !pInfo[playerid][player_gym_can_workout] ) return;

			if(pInfo[playerid][player_gym_rep_next_start] > GetTickCount())
			{
				GameTextForPlayer(playerid, "~r~zbyt wczesnie na kolejne wycisniecie", 1000, 3);
				return;
			}

			pInfo[playerid][player_gym_is_pushing] = true;

			ApplyAnimation( playerid, "benchpress", "gym_bp_up_A", 1, 0, 0, 0, 1, 0, 1 );
			pInfo[playerid][player_gym_rep_start_tick] = GetTickCount();
		}

		if( RELEASED(KEY_JUMP) )
		{
			if( !pInfo[playerid][player_gym_can_workout] ) return;
			if( !pInfo[playerid][player_gym_is_pushing] ) return;
			if( GetTickCount() - pInfo[playerid][player_gym_rep_start_tick] < 2100 )
			{
				GameTextForPlayer(playerid, "~r~sztanga nie zostala wycisnieta do konca", 1000, 3);
			}
			else
			{
				// wycisnieta do konca
				pInfo[playerid][player_gym_reps]++;
				PlayerTextDrawSetString(playerid, pInfo[playerid][gymTd], sprintf("Wycisniec: ~y~%d~n~~n~~w~Przytrzymaj LSHIFT az do calkowitego wycisniecia sztangi 100 razy, aby zwiekszyc sile.~n~~n~Niektore narkotyki zwiekszaja szybkosc zdobywania sily, a inne czestotliwosc treningow.", pInfo[playerid][player_gym_reps]));
			}

			pInfo[playerid][player_gym_is_pushing] = false;

			pInfo[playerid][player_gym_rep_next_start] = GetTickCount() + 1300;
			ApplyAnimation( playerid, "benchpress", "gym_bp_down", 1, 0, 0, 0, 1, 0, 1 );
		}
	}
}

timer BenchStart[100](playerid)
{
	new slot = GetPlayerFreeAttachSlot(playerid);
	if(slot != -1)
	{
		SetPlayerAttachedObject(playerid, slot, 2913, 6);
		pInfo[playerid][player_gym_attach_slot] = slot;
	}

	pInfo[playerid][player_gym_can_workout] = true;
	PlayerTextDrawSetString(playerid, pInfo[playerid][gymTd], "Wycisniec: ~y~0~n~~n~~w~Przytrzymaj LSHIFT az do calkowitego wycisniecia sztangi 100 razy, aby zwiekszyc sile.~n~~n~Niektore narkotyki zwiekszaja szybkosc zdobywania sily, a inne czestotliwosc treningow.");
	PlayerTextDrawShow(playerid, pInfo[playerid][gymTd]);
}

timer BenchBeforeStop[100](playerid)
{
	if(pInfo[playerid][player_gym_attach_slot] > -1) RemovePlayerAttachedObject(playerid, pInfo[playerid][player_gym_attach_slot]);
}

timer BenchStop[100](playerid)
{
	TogglePlayerControllable(playerid, 1);
	SetCameraBehindPlayer(playerid);

	pInfo[playerid][player_is_gym_training] = false;
	pInfo[playerid][player_gym_object] = -1;
}